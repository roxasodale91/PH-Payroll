import { DeductionComponent } from './components/Maintenance/Deduction/Deduction.component';
import { HomeComponent } from './components/home/home.component';
import { ProcesspayrollComponent } from './components/transaction/processpayroll/processpayroll.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { TimekeepingComponent } from './components/transaction/timekeeping/timekeeping.component';
import { EarningsComponent } from './components/transaction/earnings/earnings.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, Component } from '@angular/core';
import { CompanyComponent } from './components/company/company.component';
import {AreaComponent} from './components/company/area/area.component';
import { MaintenanceComponent } from './components/Maintenance/Maintenance.component';
import { DailyRateComponent } from './components/Maintenance/Statutory/DailyRate/DailyRate.component';
import { PagibigComponent } from './components/Maintenance/Statutory/Pagibig/Pagibig.component';
import { PhilhealthComponent } from './components/Maintenance/Statutory/Philhealth/Philhealth.component';
import { SSSComponent } from './components/Maintenance/Statutory/SSS/SSS.component';
import { WithholdingComponent } from './components/Maintenance/Statutory/Withholding/Withholding.component';
import { StatutoryComponent } from './components/Maintenance/Statutory/Statutory.component';
import { DailyScheduleComponent } from './components/Maintenance/DailySchedule/DailySchedule.component';
import { HolidaySetupComponent } from './components/Maintenance/HolidaySetup/HolidaySetup.component';
import { EmployeesComponent } from './components/company/employees/employees.component';
import { OutletComponent } from './components/company/outlet/outlet.component';
import { DepartmentComponent } from './components/company/department/department.component';
import { DivisionComponent } from './components/company/division/division.component';
import { PositionComponent } from './components/company/position/position.component';
import { SalarytableComponent } from './components/company/salarytable/salarytable.component';
import { LockunlockpayperiodComponent } from './components/company/lockunlockpayperiod/lockunlockpayperiod.component';
import { ProfileComponent } from './components/company/profile/profile.component';
import { UserAccountComponent } from './components/Maintenance/UserAccount/UserAccount.component';
import { LeaveBalancesComponent } from './components/Maintenance/LeaveBalances/LeaveBalances.component';
import { DailyleaveComponent } from './components/transaction/dailyleave/dailyleave.component';
import { TransactiondeductionComponent } from './components/transaction/transactiondeduction/transactiondeduction.component';

const router: Routes = [
  {path: 'company',
  component: CompanyComponent,
  children: [
    {path: 'area', component: AreaComponent},
    {path: 'department', component: DepartmentComponent},
    {path: 'division', component: DivisionComponent},
    {path: 'employees', component: EmployeesComponent},
    {path: 'lockunlockpayperiod', component: LockunlockpayperiodComponent},
    {path: 'outlet', component: OutletComponent},
    {path: 'position', component: PositionComponent},
    {path: 'salarytable', component: SalarytableComponent},
    {path: 'profile', component: ProfileComponent}
  ]},
  {
    path: 'maintenance',
    component: MaintenanceComponent,
    children: [
    {
      path: 'statutory',
      component: StatutoryComponent,
      children:
      [
        {
          path: 'dailyrate',
          component: DailyRateComponent
        },
        {
          path: 'pagibig',
          component: PagibigComponent
        },
        {
          path: 'philhealth',
          component: PhilhealthComponent
        },
        {
          path: 'sss',
          component: SSSComponent
        },
        {
          path: 'withholding',
          component: WithholdingComponent
        }
      ]
    },
    {
      path: 'deduction',
      component: DeductionComponent
    },
    {
      path: 'dailyschedule',
      component: DailyScheduleComponent
    },
    {
      path: 'holidaysetup',
      component: HolidaySetupComponent
    },
    {
      path: 'leavebalances',
      component: LeaveBalancesComponent
    },
    {
      path: 'useraccount',
      component: UserAccountComponent
    }
  ]},
  {path: 'transaction',
  component: TransactionComponent,
  children: [
    {path: 'dailyleave', component: DailyleaveComponent},
    {path: 'earnings', component: EarningsComponent},
    {path: 'processpayroll', component: ProcesspayrollComponent},
    {path: 'timekeeping', component: TimekeepingComponent},
    {path: 'transactiondeduction', component: TransactiondeductionComponent}
  ]},
  // Redirect to Home
  {path: '**', component: HomeComponent}
];


export const routes: ModuleWithProviders  = RouterModule.forRoot(router);
export const appRoutes = RouterModule.forChild(router);
