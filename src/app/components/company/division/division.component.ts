import { Component, OnInit, Attribute, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { CompanyDivision, CompanyDivisionProps, CompanyDivisionViewModel } from '../../../models/division.model';
import { FormInterface } from '../../../inteface/form.interface';
import { DivisionService } from '../../../services/division.service';

@Component({
  selector: 'app-division',
  templateUrl: './division.component.html',
  styleUrls: ['./division.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DivisionComponent implements OnInit, FormInterface<CompanyDivision> {
  form = new FormGroup({});
  model = new CompanyDivision;
  properties = new CompanyDivisionProps;
  viewmodel = new CompanyDivisionViewModel;
  options: FormlyFormOptions = {};
  values: CompanyDivision[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description',
          }
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note',
          }
        },
        {
          type: 'input',
          key: this.properties.levelId.toString(),
          templateOptions: {
            label: 'Level',
          }
        },
        {
          type: 'input',
          key: this.properties.salaryId.toString(),
          templateOptions: {
            label: 'Salary Grade',
            type: 'number'
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanyDivision[] = [];
  toggleItem(event, object: CompanyDivision) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.divisionservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.divisionservice.addData(this.model);
    this.model = new CompanyDivision;
  }
  constructor(private divisionservice: DivisionService) { }

  ngOnInit() {
    this.divisionservice.all
    .subscribe((data: CompanyDivision[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
