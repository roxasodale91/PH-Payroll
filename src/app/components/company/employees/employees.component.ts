import { ExtensionService } from '../../../extensions/formatter.extension';
import { Observable } from 'rxjs';
import { EmployeeViewModel, Employee, EmployeeProps } from '../../../models/employee.model';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { AreaService } from '../../../services/area.service';
import { CompanyArea } from '../../../models/area.model';
import { FormInterface } from '../../../inteface/form.interface';
import { EmployeesService } from '../../../services/employees.service';
import { TabType } from '../../../inteface/tabform.interface';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeesComponent implements OnInit, FormInterface<Employee> {
  fields: FormlyFieldConfig[];
  form = new FormGroup({});
  model = new Employee;
  properties = new EmployeeProps;
  viewmodel = new EmployeeViewModel;
  options: FormlyFormOptions = {};
  values: Employee[];

  areas: Observable<CompanyArea[]> = this.areaservice.all;
  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
          {
            type: 'input',
            key: this.properties.empId.toString(),
            templateOptions: {
              label: 'Employee ID'
            }
          },
        {
          type: 'input',
          key: this.properties.password.toString(),
          templateOptions: {
            type: 'Password',
            label: 'Password '
          }
        },
        {
          type: 'input',
          key: this.properties.prevId.toString(),
          templateOptions: {
            label: 'Prev Emp ID'
          }
        },
        {
          type: 'input',
          key: this.properties.lastName.toString(),
          templateOptions: {
            label: 'Last Name'
          }
        },
        {
          type: 'input',
          key: this.properties.middleName.toString(),
          templateOptions: {
            label: 'Middle Name'
          }
        },
        {
          type: 'input',
          key: this.properties.firstName.toString(),
          templateOptions: {
            label: 'First Name'
          }
        },
        {
          type: 'select',
          key: this.properties.suffixName.toString(),
          templateOptions: {
            label: 'Suffix Name',
            options: [
              { label: 'SR', value: 'sr' },
              { label: 'JR', value: 'jr' },
              { label: 'III', value: 'third' }
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.birthday.toString(),
          templateOptions: {
            label: 'Birthday',
            type: 'date'
          }
        },
        {
          type: 'select',
          key: this.properties.gender.toString(),
          templateOptions: {
            label: 'Gender',
            options: [
              { label: 'Male', value: 'male' },
              { label: 'Female', value: 'female' }
            ]
          }
        },
        {
          type: 'select',
          key: this.properties.civilStatus.toString(),
          templateOptions: {
            label: 'Civil Status',
            options: [
              { label: 'Single', value: 'single' },
              { label: 'Married', value: 'married' },
              { label: 'Divorced', value: 'divorced' },
              { label: 'Widow', value: 'widow' }
            ]
          }
        }
      ]
        }
      ]
    },
    {
      label: 'Designation',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'select',
              key: this.properties.company.toString(),
              templateOptions: {
                label: 'Company',
                options: [
                  { label: 'EXCELLENT PEOPLE\'S MPC (OC)', value: 'option_1' }
                ]
              }
            },
            {
              type: 'select',
              key: this.properties.outlet.toString(),
              templateOptions: {
                label: 'Outlet',
                options: [{ label: 'SHELL OPTIMUS', value: 'option_1' }]
              }
            },
            {
              type: 'select',
              key: this.properties.area.toString(),
              templateOptions: {
                label: 'Area',
                options: this.extension.formatted
              }
            },
            {
              type: 'select',
              key: this.properties.department.toString(),
              templateOptions: {
                label: 'Department',
                options: [{ label: 'NONE', value: 'option_1' }]
              }
            },
            {
              type: 'select',
              key: this.properties.division.toString(),
              templateOptions: {
                label: 'Division',
                options: [{ label: 'NONE', value: 'option_1' }]
              }
            },
            {
              type: 'select',
              key: this.properties.position.toString(),
              templateOptions: {
                label: 'Position',
                options: [{ label: 'RESTAURANT STAFF', value: 'option_1' }]
              }
            }
          ]
        }
      ]
    },
    {
      label: 'Payroll Status',
      fields: [
        {
          fieldGroupClassName: '',
          fieldGroup: [
            {
              fieldGroup: [
                {
                  type: 'input',
                  key: this.properties.payPeriod.toString(),
                  templateOptions: {
                  label: 'Pay Period'
                  }
                }
              ],
            },
            {
              className: 'section-label',
              template: '<br><div><strong>Salary Rate</strong></div><hr />'
            },
            {
              type: 'select',
              key: this.properties.billingRate.toString(),
              templateOptions: {
                label: 'Billing Rate',
                options: [{ label: 'SAMPLE OPTION', value: 'option_1' }]
              }
            },
            {
              type: 'select',
              key: this.properties.workSchedule.toString(),
              templateOptions: {
                label: 'Work Schedule',
                options: [{ label: '-n/a-', value: 'option_1' }]
              }
            },
            // {
            //   type: 'select',
            //   key: this.properties.rateTable.toString(),
            //   templateOptions: {
            //     label: 'Rate Table',
            //     options: [{ label: 'SHELL OPTIMUS', value: 'option_1' }]
            //   }
            // },
            // {
            //   fieldGroupClassName: 'uk-grid uk-child-width-1-3@m',
            //   fieldGroup: [
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.rateMonthly.toString(),
            //     //   templateOptions: {
            //     //     label: 'Monthly'
            //     //   }
            //     // },
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.rateDaily.toString(),
            //     //   templateOptions: {
            //     //     label: 'Daily'
            //     //   }
            //     // },
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.rateHourly.toString(),
            //     //   templateOptions: {
            //     //     label: 'Hourly',
            //     //     disabled: true
            //     //   }
            //     // },
            //   ],
            // },
            // {
            //   className: 'section-label ',
            //   template: '<br><div><strong>Employment Status</strong></div><hr />'
            // },
            // {
            //   type: 'select',
            //   key: this.properties.empStatus.toString(),
            //   templateOptions: {
            //     options: [{ label: 'Regular', value: 'option_1' }]
            //   }
            // },
            // {
            //   fieldGroupClassName: 'uk-grid uk-child-width-1-3@m',
            //   fieldGroup: [
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.dateHired.toString(),
            //     //   templateOptions: {
            //     //     label: 'Date Hired',
            //     //     type: 'Date'
            //     //   }
            //     // },
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.dateRegularized.toString(),
            //     //   templateOptions: {
            //     //     label: 'Date Regularized',
            //     //     type: 'Date'
            //     //   }
            //     // },
            //     // {
            //     //   type: 'input',
            //     //   key: this.properties.dateResigned.toString(),
            //     //   templateOptions: {
            //     //     label: 'Date Resigned',
            //     //     type: 'Date'
            //     //   }
            //     // }
            //   ],
            // },
            // {
            //   className: 'col-6',
            //   type: 'input',
            //   key: this.properties.remarks.toString(),
            //   templateOptions: {
            //     label: 'Remarks'
            //   }
            // }
          ]
        }
      ]
    },
    // {
    //   label: 'Government',
    //   fields: [
    //     {
    //       fieldGroup: [
    //         {
    //           type: 'input',
    //           key: this.properties.bankAccountNumber.toString(),
    //           templateOptions: {
    //             label: 'Bank Account',
    //             type: 'number'
    //           }
    //         }
    //       ]
    //     },
    //     {
    //       fieldGroupClassName: 'row',
    //       fieldGroup: [
    //         {
    //           className: 'col-3',
    //           type: 'checkbox',
    //           key: this.properties.holdBankProcess.toString(),
    //           templateOptions: {
    //             label: 'Hold Bank Process'
    //           }
    //         },
    //         {
    //           className: 'col-9',
    //           type: 'select',
    //           key: this.properties.holdBankReason.toString(),
    //           templateOptions: {
    //             options: [{ label: 'In-Active', value: 'option_1' }]
    //           }
    //         }
    //       ]
    //     },
    //     {
    //       fieldGroup: [
    //         {
    //           type: 'input',
    //           key: this.properties.sssNumber.toString(),
    //           templateOptions: {
    //             label: 'SSS Number',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           type: 'input',
    //           key: this.properties.philHealthNumber.toString(),
    //           templateOptions: {
    //             label: 'Phil-Health',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           type: 'input',
    //           key: this.properties.pagibigNumber.toString(),
    //           templateOptions: {
    //             label: 'Pag-Ibig',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           className: 'section-label',
    //           template: '<br><div><strong>Tax Properties</strong></div><hr />'
    //         },
    //         {
    //           type: 'select',
    //           key: this.properties.taxStatus.toString(),
    //           templateOptions: {
    //             label: 'Tax Status',
    //             options: [{ label: 'TE - TAX EXEMPTED', value: 'option_1' }]
    //           }
    //         },
    //         {
    //           type: 'input',
    //           key: this.properties.dependent.toString(),
    //           templateOptions: {
    //             label: 'Dependent',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           type: 'input',
    //           key: this.properties.taxId.toString(),
    //           templateOptions: {
    //             label: 'Tax ID',
    //             type: 'number'
    //           }
    //         }
    //       ]
    //     }
    //   ]
    // },
    // {
    //   label: 'Contacts',
    //   fields: [
    //     {
    //       fieldGroupClassName: 'row',
    //       fieldGroup: [
    //         {
    //           className: 'col-6',
    //           type: 'input',
    //           key: this.properties.mobileNumber.toString(),
    //           templateOptions: {
    //             label: 'Mobile Number',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           className: 'col-6',
    //           type: 'input',
    //           key: this.properties.telNumber.toString(),
    //           templateOptions: {
    //             label: 'Telephone Number',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           className: 'col-6',
    //           type: 'input',
    //           key: this.properties.email.toString(),
    //           templateOptions: {
    //             label: 'E-mail Address',
    //             type: 'email'
    //           }
    //         },
    //         {
    //           className: 'col-6',
    //           type: 'input',
    //           key: this.properties.parentGuardian.toString(),
    //           templateOptions: {
    //             label: 'Parent/Guardian',
    //             type: 'number'
    //           }
    //         },
    //         {
    //           className: 'col-6',
    //           type: 'textarea',
    //           key: this.properties.presentAddress.toString(),
    //           templateOptions: {
    //             label: 'Present Address'
    //           }
    //         },
    //         {
    //           className: 'col-6',
    //           type: 'textarea',
    //           key: this.properties.permanentAddress.toString(),
    //           templateOptions: {
    //             label: 'Permanent Address'
    //           }
    //         }
    //       ]
    //     }
    //   ]
    // },
  ];

  toggledAll: boolean;
  aSelectedItems: Employee[] = [];
  toggleItem(event, object: Employee) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.employeesservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.employeesservice.addData(this.model);
    this.model = new Employee;
  }

  constructor(private employeesservice: EmployeesService, private areaservice: AreaService, private extension: ExtensionService) {
  }

  ngOnInit() {
    this.extension.format(this.areas);
    this.employeesservice.all
    .subscribe((data: Employee[]) => {
      this.values = data;
      console.log(data);
    });
  }
}
