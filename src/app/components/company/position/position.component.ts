import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormInterface } from '../../../inteface/form.interface';
import { CompanyPosition, CompanyPositionProps } from '../../../models/position.model';
import { CompanyAreaViewModel } from '../../../models/area.model';
import { PositionService } from '../../../services/position.service';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PositionComponent implements OnInit, FormInterface<CompanyPosition> {
  form = new FormGroup({});
  model = new CompanyPosition;
  properties = new CompanyPositionProps;
  viewmodel = new CompanyAreaViewModel;
  options: FormlyFormOptions = {};
  values: CompanyPosition[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID',
            disabled: true
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.role.toString(),
          templateOptions: {
            label: 'Role',
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanyPosition[] = [];
  toggleItem(event, object: CompanyPosition) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.positionservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.positionservice.addData(this.model);
    this.model = new CompanyPosition;
  }

  constructor(private positionservice: PositionService) { }

  ngOnInit() {
    this.positionservice.all
    .subscribe((data: CompanyPosition[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
