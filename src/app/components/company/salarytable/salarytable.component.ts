import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormInterface } from '../../../inteface/form.interface';
import { CompanySalaryTable, CompanySalaryTableProps, CompanySalaryTableViewModel } from '../../../models/salarytable.model';
import { SalaryTableService } from '../../../services/salarytable.service';

@Component({
  selector: 'app-salarytable',
  templateUrl: './salarytable.component.html',
  styleUrls: ['./salarytable.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SalarytableComponent implements OnInit, FormInterface<CompanySalaryTable> {
  form = new FormGroup({});
  model = new CompanySalaryTable;
  properties = new CompanySalaryTableProps;
  viewmodel = new CompanySalaryTableViewModel;
  options: FormlyFormOptions = {};
  values: CompanySalaryTable[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID',
            disabled: true
          },
        },
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description',
          }
        },
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note',
          }
        },
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.rateMonthly.toString(),
          templateOptions: {
            label: 'Monthly Rate',
          }
        },
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.rateDaily.toString(),
          templateOptions: {
            label: 'Daily Rate',
          }
        },
        {
          className: 'col-6',
          type: 'input',
          key: this.properties.rateHourly.toString(),
          templateOptions: {
            label: 'Hourly Rate',
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanySalaryTable[] = [];
  toggleItem(event, object: CompanySalaryTable) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.salarytableservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.salarytableservice.addData(this.model);
    this.model = new CompanySalaryTable;
  }

  constructor(private salarytableservice: SalaryTableService) { }

  ngOnInit() {
    this.salarytableservice.all
    .subscribe((data: CompanySalaryTable[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
