/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SalarytableComponent } from './salarytable.component';

describe('SalarytableComponent', () => {
  let component: SalarytableComponent;
  let fixture: ComponentFixture<SalarytableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalarytableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalarytableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
