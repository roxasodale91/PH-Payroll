import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, Component } from '@angular/core';
import { CompanyComponent } from './company.component';
import {AreaComponent} from './area/area.component';

const router: Routes = [
  // {path: '/area', component: AreaComponent},
  // // Redirect to Home
  // {path: '**', component: CompanyComponent}
];


export const routes: ModuleWithProviders  = RouterModule.forRoot(router);
export const appRoutes = RouterModule.forChild(router);
