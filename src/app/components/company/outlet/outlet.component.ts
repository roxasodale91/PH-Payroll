import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { CompanyOutlet, CompanyOutletProps, CompanyOutletViewModel } from '../../../models/outlet.model';
import { FormInterface } from '../../../inteface/form.interface';
import { OutletService } from '../../../services/outlet.service';

@Component({
  selector: 'app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OutletComponent implements OnInit, FormInterface<CompanyOutlet> {
  form = new FormGroup({});
  model = new CompanyOutlet;
  properties = new CompanyOutletProps;
  viewmodel = new CompanyOutletViewModel;
  options: FormlyFormOptions = {};
  values: CompanyOutlet[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID',
            disabled: true
          },
        },
        {
          type: 'input',
          key: this.properties.storeCode.toString(),
          templateOptions: {
            label: 'Store Code',
          }
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description',
          }
        },
        {
          type: 'select',
          key: this.properties.areaId.toString(),
          templateOptions: {
            label: 'Area',
            options: [
              {label: 'VALENZUELA', value: 'option1'}
            ]
          }
        },
        {
          type: 'select',
          key: this.properties.corporateNameId.toString(),
          templateOptions: {
            label: 'Corporate Name',
            options: [
              {label: 'SHAKEYS', value: 'option1'}
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.eclLowLimit.toString(),
          templateOptions: {
            label: 'ECOLA Lower Limit',
            type: 'number'
          }
        },
        {
          type: 'input',
          key: this.properties.eclUpperLimit.toString(),
          templateOptions: {
            label: 'ECOLA Upper Limit',
            type: 'number'
          }
        },
        {
          type: 'input',
          key: this.properties.eclDayAmount.toString(),
          templateOptions: {
            label: 'ECOLA Rate',
            type: 'number'
          }
        },
        {
          type: 'input',
          key: this.properties.eclMinWage.toString(),
          templateOptions: {
            label: 'Daily Rate',
            type: 'number'
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanyOutlet[] = [];
  toggleItem(event, object: CompanyOutlet) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.outletservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.outletservice.addData(this.model);
    this.model = new CompanyOutlet;
  }

  constructor(private outletservice: OutletService) { }

  ngOnInit() {
    this.outletservice.all
    .subscribe((data: CompanyOutlet[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
