import { ModalService } from './../../../services/modal.service';
import { FormInterface } from '../../../inteface/form.interface';
import { CompanyAreaViewModel, CompanyArea, CompanyAreaProps } from '../../../models/area.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, NgModel } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { AreaService } from '../../../services/area.service';
import { map } from 'rxjs/operators';
import { forEach } from '../../../../../node_modules/@angular/router/src/utils/collection';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit, FormInterface<CompanyArea> {

  properties = new CompanyAreaProps;
  viewmodel = new CompanyAreaViewModel;
  model = new CompanyArea;
  form = new FormGroup({});
  options: FormlyFormOptions = {};

  values: CompanyArea[];
  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description,
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.location,
          templateOptions: {
            label: 'Location',
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanyArea[] = [];
  toggleItem(event, object: CompanyArea) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.areaservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.areaservice.addData(this.model);
    this.model = new CompanyArea;
  }

  take(id: string) {
    const filter = this.values.filter(c => c.id === id)[0];
    this.model = filter;
    console.log(filter);
  }

  constructor(private areaservice: AreaService, private modalservice: ModalService) { }

  ngOnInit() {
     this.areaservice.all
     .subscribe((data: CompanyArea[]) => {
       this.values = data;
       console.log(data);
      });
  }

}
