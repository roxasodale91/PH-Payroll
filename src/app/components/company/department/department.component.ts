import { DepartmentService } from '../../../services/department.service';
import { FormInterface } from '../../../inteface/form.interface';
import { CompanyDepartment, CompanyDepartmentProps, CompanyDepartmentViewModel } from '../../../models/department.model';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { CompanyAreaViewModel } from '../../../models/area.model';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DepartmentComponent implements OnInit, FormInterface<CompanyDepartment> {
  form = new FormGroup({});
  model = new CompanyDepartment;
  options: FormlyFormOptions = {};
  properties = new CompanyDepartmentProps;
  viewmodel = new CompanyDepartmentViewModel;
  values: CompanyDepartment[];
  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID',
          },
        },
        {
          type: 'input',
          key: this.properties.description,
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.note,
          templateOptions: {
            label: 'Note',
          }
        },
        {
          type: 'select',
          key: this.properties.outletId.toString(),
          templateOptions: {
            label: 'Outlet',
            options: [
              {label: 'SHAKEYS MALATE', value: 'option1'}
            ]
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: CompanyDepartment[] = [];
  toggleItem(event, object: CompanyDepartment) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.departmentService.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.departmentService.addData(this.model);
    this.model = new CompanyDepartment;
  }

  constructor(private departmentService: DepartmentService) { }

  ngOnInit() {
    this.departmentService.all
    .subscribe((data: CompanyDepartment[]) => {
      this.values = data;
    });
  }

}
