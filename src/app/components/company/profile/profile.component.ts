import { ModalService } from './../../../services/modal.service';
import { TabType } from '../../../inteface/tabform.interface';
import { CompanyProps, CompanyViewModel } from '../../../models/company.model';
import { ProfileService } from '../../../services/profile.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { Company } from '../../../models/company.model';
import { FormInterface } from '../../../inteface/form.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, FormInterface<Company> {
  form = new FormGroup({});
  model = new Company();
  properties = new CompanyProps();
  viewmodel = new CompanyViewModel();
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  values: Company[];
  modaltitle = 'custom-modal-1';

  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.id.toString(),
              templateOptions: {
                label: 'ID'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.companyName.toString(),
              templateOptions: {
                label: 'Company Name'
              }
            }
          ]
        }
      ]
    },
    {
      label: 'Contacts',
      fields: [
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.mobileNo.toString(),
              templateOptions: {
                label: 'Mobile Number',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.telephoneNo.toString(),
              templateOptions: {
                label: 'Telephone Number',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.faxNo.toString(),
              templateOptions: {
                label: 'Fax Number',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.email.toString(),
              templateOptions: {
                label: 'E-mail address',
                type: 'email'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.contactPerson.toString(),
              templateOptions: {
                label: 'Contact Person'
              }
            }
          ]
        }
      ]
    },
    {
      label: 'Licenses',
      fields: [
        {
          fieldGroup: [
            {
              className: 'section-label',
              template: '<br><div><strong>Account Number</strong></div><hr />'
            }
          ]
        },
        {
          model: this.model,
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.sss.toString(),
              templateOptions: {
                label: 'SSS',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.philHealth.toString(),
              templateOptions: {
                label: 'Phil-Health',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.tin.toString(),
              templateOptions: {
                label: 'Tax id',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.pagIbig.toString(),
              templateOptions: {
                label: 'Pag-Ibig',
                type: 'number'
              }
            },
            {
              className: 'col-6',
              type: 'input',
              key: this.properties.licenseNo.toString(),
              templateOptions: {
                label: 'License Number',
                type: 'number'
              }
            }
          ]
        }
      ]
    },
    {
      label: 'Reference Earnings/Deductions',
      fields: [
        {
          model: this.model,
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDSSS.toString(),
              templateOptions: {
                label: 'SSS',
                options: [{ label: 'SSS CONTRIBUTION', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.schedSSS.toString(),
              templateOptions: {
                label: 'SSS Sched',
                options: [{ label: 'End', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDRegDay.toString(),
              templateOptions: {
                label: 'Regular Day',
                options: [{ label: 'REGULAR DAY', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDPhilHealth.toString(),
              templateOptions: {
                label: 'Phil-Health',
                options: [{ label: 'PHILHEALTH', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.schedPhilHealth.toString(),
              templateOptions: {
                label: 'PhilHealth Sched',
                options: [{ label: 'End', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDLegHoliday.toString(),
              templateOptions: {
                label: 'Legal Holiday',
                options: [{ label: 'LEGAL HOLidAY', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'checkbox',
              key: this.properties.isdailyRateFactor.toString(),
              templateOptions: {
                label: 'Enable Daily Rate Factor'
              }
            },
            {
              className: 'col-4',
              type: 'input',
              key: this.properties.dailyRateFactor.toString(),
              templateOptions: {
                label: 'Daily Rate Factor',
                type: 'number'
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDSpecHoliday.toString(),
              templateOptions: {
                label: 'Special Holiday',
                options: [{ label: 'SPECIAL HOLidAY', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDTax.toString(),
              templateOptions: {
                label: 'W/Holding Tax',
                options: [{ label: 'W/HOLDING TAX', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.schedTax.toString(),
              templateOptions: {
                label: 'W/Holding Tax Sched',
                options: [{ label: 'Semi', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDUnWorkHoliday.toString(),
              templateOptions: {
                label: 'Unworked Holiday',
                options: [{ label: 'UNWORKED HOLidAY', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDPagIbig.toString(),
              templateOptions: {
                label: 'Pag-Ibig (HMO)',
                options: [{ label: 'PAG-IBIG CONTRIBUTION', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.schedPagIbig.toString(),
              templateOptions: {
                label: 'Pag-Ibig Sched',
                options: [{ label: 'Middle', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.refIDEcola.toString(),
              templateOptions: {
                label: 'ECOLA',
                options: [{ label: 'ECOLA', value: 'option_1' }]
              }
            },
            {
              className: 'col-4',
              type: 'select',
              key: this.properties.schedECOLA.toString(),
              templateOptions: {
                label: 'ECOLA Sched',
                options: [{ label: 'Semi', value: 'option_1' }]
              }
            }
          ]
        }
      ]
    }
  ];

  toggledAll: boolean;
  aSelectedItems: Company[] = [];
  toggleItem(event, object: Company) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.profileservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  constructor(
    private profileservice: ProfileService,
    private modalservice: ModalService
  ) {}

  ngOnInit() {
    this.profileservice.all.subscribe((data: Company[]) => {
      this.values = data;
      console.log(data);
    });
  }

  take(id: string) {
    const filter = this.values.filter(c => c.id === id)[0];
    this.model = filter;
    console.log(filter);
  }

  submit() {
    console.log(this.model);
    this.profileservice.addData(this.model);
    this.model = new Company();
  }
}
