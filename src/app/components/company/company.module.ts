import { ModalComponent } from './../../directives/modal/modal.component';
import { AreaService } from '../../services/area.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import { CommonModule } from '@angular/common';
import { CompanyComponent } from './company.component';
import { AreaComponent } from './area/area.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component';
import { OutletComponent } from './outlet/outlet.component';
import { DepartmentComponent } from './department/department.component';
import { DivisionComponent } from './division/division.component';
import { PositionComponent } from './position/position.component';
import { SalarytableComponent } from './salarytable/salarytable.component';
import { LockunlockpayperiodComponent } from './lockunlockpayperiod/lockunlockpayperiod.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileService } from '../../services/profile.service';
import { HttpClientModule } from '@angular/common/http';
import './../../app.component.css';

@NgModule({ // imports - for sub module
  imports: [
    RouterModule,
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    HttpClientModule,
  ], // declarations - for component
  declarations: [
    CompanyComponent,
    AreaComponent,
    DepartmentComponent,
    EmployeesComponent,
    OutletComponent,
    DivisionComponent,
    PositionComponent,
    SalarytableComponent,
    LockunlockpayperiodComponent,
    ProfileComponent,
    ModalComponent,
    HttpClientModule
  ],
  providers: [
    // ProfileService,
    // AreaService
  ]
})
export class CompanyModule { }
