import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormInterface } from '../../../inteface/form.interface';
import { LockUnlock, LockUnlockProps, LockUnlockViewmodel } from '../../../models/lockunlock.model';
import { LockUnlockService } from '../../../services/lockunlock.service';

@Component({
  selector: 'app-lockunlockpayperiod',
  templateUrl: './lockunlockpayperiod.component.html',
  styleUrls: ['./lockunlockpayperiod.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LockunlockpayperiodComponent implements OnInit, FormInterface<LockUnlock> {
  form = new FormGroup({});
  model = new LockUnlock;
  properties = new LockUnlockProps;
  viewmodel = new LockUnlockViewmodel;
  options: FormlyFormOptions = {};
  values: LockUnlock[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.payPeriod.toString(),
          templateOptions: {
            label: 'Pay Period',
            // disabled: true
          },
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note',
          }
        },
        {
          type: 'checkbox',
          key: this.properties.isLocked.toString(),
          templateOptions: {
            label: 'Locked',
          }
        },
      ],
    },
  ];

  toggledAll: boolean;
  aSelectedItems: LockUnlock[] = [];
  toggleItem(event, object: LockUnlock) {
    if (event.target.checked) {
      this.aSelectedItems.push(object);
    } else {
      const index: number = this.aSelectedItems.indexOf(object);
      if (index !== -1) {
        this.aSelectedItems.splice(index, 1);
      }
    }
  }

  toggleAll(event) {
    if (event.target.checked) {
      this.toggledAll = true;
      for (let i = 0; i < this.values.length; i++) {
        this.aSelectedItems.push(this.values[i]);
      }
    } else {
      this.toggledAll = false;
      this.aSelectedItems = [];
    }
  }

  deleteItem() {
    for (let i = 0; i < this.aSelectedItems.length; i ++) {
      this.lockunlockservice.deleteData(this.aSelectedItems[i]);
    }
    this.toggledAll = false;
  }

  submit() {
    this.lockunlockservice.addData(this.model);
    this.model = new LockUnlock;
  }

  constructor(private lockunlockservice: LockUnlockService) { }

  ngOnInit() {
    this.lockunlockservice.all
    .subscribe((data: LockUnlock[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
