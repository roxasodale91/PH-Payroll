/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LockunlockpayperiodComponent } from './lockunlockpayperiod.component';

describe('LockunlockpayperiodComponent', () => {
  let component: LockunlockpayperiodComponent;
  let fixture: ComponentFixture<LockunlockpayperiodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LockunlockpayperiodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockunlockpayperiodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
