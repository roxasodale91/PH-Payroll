import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-useraccess',
  templateUrl: './useraccess.component.html',
  styleUrls: ['./useraccess.component.css']
})
export class UseraccessComponent implements OnInit {

  loggedIn: string | boolean;

  constructor() { }

  ngOnInit() {
    console.log(localStorage.getItem('loggedIn').toString());
    this.loggedIn = localStorage.getItem('loggedIn').toString();
  }

  submit() {
    this.loggedIn = false;
    localStorage.setItem('loggedIn', this.loggedIn.toString());

    console.log(localStorage.getItem('loggedIn').toString());
  }
}
