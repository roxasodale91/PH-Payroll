import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { Deduction, DeductionProps, DeductionViewModel } from '../../../models/deduction.model';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { DeductionService } from '../../../services/deduction.service';
import { TabType } from './../../../inteface/tabform.interface';

@Component({
  selector: 'app-deduction',
  templateUrl: './Deduction.component.html',
  styleUrls: ['./Deduction.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DeductionComponent implements OnInit, FormInterface<Deduction> {
  form = new FormGroup({});
  model = new Deduction;
  properties = new DeductionProps;
  viewmodel = new DeductionViewModel;
  options: FormlyFormOptions = {};
  values: Deduction[];
  fields: FormlyFieldConfig[];

  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'input',
              key: this.properties.payPeriod.toString(),
              templateOptions: {
                label: 'Pay Period'
              },
            },
            {
              type: 'input',
              key: this.properties.fullName.toString(),
              templateOptions: {
                label: 'Employee Name'
              },
            },
            {
              type: 'input',
              key: this.properties.companyId.toString(),
              templateOptions: {
                label: 'Company'
              },
            },
            {
              type: 'input',
              key: this.properties.outletName.toString(),
              templateOptions: {
                label: 'Outlet'
              },
            },
            {
              type: 'input',
              key: this.properties.txDate.toString(),
              templateOptions: {
                label: 'Posting Date',
                type: 'date'
              },
            },
            {
              type: 'input',
              key: this.properties.loanDate.toString(),
              templateOptions: {
                label: 'Loan Date',
                type: 'date'
              },
            },
            {
              type: 'input',
              key: this.properties.deductionId.toString(),
              templateOptions: {
                label: 'Deduction Type'
              },
            },
            {
              type: 'input',
              key: this.properties.deductionType.toString()
            },
            {
              type: 'input',
              key: this.properties.scheduleMode.toString(),
              templateOptions: {
                label: 'Terms / Schedule'
              },
            },
            {
              type: 'input',
              key: this.properties.amount.toString(),
              templateOptions: {
                label: 'Amount'
              },
            },
            {
              type: 'input',
              key: this.properties.note.toString(),
              templateOptions: {
                label: 'Note'
              },
            },
          ]
        }
      ]
    },
    {
      label: 'Scheduled Deduction List',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'input',
              key: this.properties.payPeriod.toString(),
              templateOptions: {
                label: 'Pay Period',
                value: '201710A'
              },
            },
            {
              type: 'input',
              key: this.properties.deductionId.toString(),
              templateOptions: {
                label: 'Type',
                value: 'REGULAR DEDUCTION'
              },
            },
            {
              type: 'input',
              key: this.properties.deductionType.toString(),
              templateOptions: {
                label: 'Type',
                value: 'SM CREDIT CARD'
              },
            },
            {
              type: 'input',
              key: this.properties.amount.toString(),
              templateOptions: {
                label: 'Type',
                value: '262.50'
              },
            },
          ]
        }
      ]
    }
  ];

  general: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.payPeriod.toString(),
          templateOptions: {
            label: 'Pay Period'
          },
        },
        {
          type: 'input',
          key: this.properties.fullName.toString(),
          templateOptions: {
            label: 'Employee Name'
          },
        },
        {
          type: 'input',
          key: this.properties.companyId.toString(),
          templateOptions: {
            label: 'Company'
          },
        },
        {
          type: 'input',
          key: this.properties.outletName.toString(),
          templateOptions: {
            label: 'Outlet'
          },
        },
        {
          type: 'input',
          key: this.properties.txDate.toString(),
          templateOptions: {
            label: 'Posting Date',
            type: 'date'
          },
        },
        {
          type: 'input',
          key: this.properties.loanDate.toString(),
          templateOptions: {
            label: 'Loan Date',
            type: 'date'
          },
        },
        {
          type: 'input',
          key: this.properties.deductionId.toString(),
          templateOptions: {
            label: 'Deduction Type'
          },
        },
        {
          type: 'input',
          key: this.properties.deductionType.toString()
        },
        {
          type: 'input',
          key: this.properties.scheduleMode.toString(),
          templateOptions: {
            label: 'Terms / Schedule'
          },
        },
        {
          type: 'input',
          key: this.properties.amount.toString(),
          templateOptions: {
            label: 'Amount'
          },
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note'
          },
        },
      ]
    }
  ];

  submit() {
    this.deductionservice.addData(this.model);
    this.model = new Deduction;
  }

  constructor(private deductionservice: DeductionService) { }

  ngOnInit() {
    this.deductionservice.all
    .subscribe((data: Deduction[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
