import { DeductionComponent } from './Deduction/Deduction.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceComponent } from './Maintenance.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StatutoryModule } from './Statutory/Statutory.module';
import { HolidaySetupComponent } from './HolidaySetup/HolidaySetup.component';
import { DailyScheduleComponent } from './DailySchedule/DailySchedule.component';
import { UserAccountComponent } from './UserAccount/UserAccount.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { LeaveBalancesComponent } from './LeaveBalances/LeaveBalances.component';
import { ModalComponent } from '../../directives/modal/modal.component';

@NgModule({
  imports: [
    FormlyModule,
    FormlyBootstrapModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations:
  [
    MaintenanceComponent,
    HolidaySetupComponent,
    LeaveBalancesComponent,
    DailyScheduleComponent,
    UserAccountComponent,
    DeductionComponent,
    ModalComponent
  ]
})
export class MaintenanceModule { }
