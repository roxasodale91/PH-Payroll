import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { Holiday, HolidayProps, HolidayViewModel } from '../../../models/holiday.model';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { HolidaySetupService } from '../../../services/holiday.service';

@Component({
  selector: 'app-holidaysetup',
  templateUrl: './HolidaySetup.component.html',
  styleUrls: ['./HolidaySetup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HolidaySetupComponent implements OnInit, FormInterface<Holiday> {
  form = new FormGroup({});
  model = new Holiday;
  properties = new HolidayProps;
  viewmodel = new HolidayViewModel;
  options: FormlyFormOptions = {};
  values: Holiday[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'select',
          key: this.properties.dailyRateId.toString(),
          templateOptions: {
            label: 'Rate',
            options: [
              {label: 'SPECIAL HOLIDAY', value: 'option1'},
              {label: 'NON WORKING HOLIDAY', value: 'option2'},
              {label: 'CHRISTMAS', value: 'option3'}
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.date.toString(),
          templateOptions: {
            label: 'Date',
            type: 'date'
          }
        },
      ]
    }
  ];

  submit() {
    this.holidaysetupservice.addData(this.model);
    this.model = new Holiday;
  }

  constructor(private holidaysetupservice: HolidaySetupService) { }

  ngOnInit() {
    this.holidaysetupservice.all
    .subscribe((data: Holiday[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
