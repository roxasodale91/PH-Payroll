import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { UserProps, UserViewModel, User } from '../../../models/user.model';
import { FormGroup, NgModel } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-useraccount',
  templateUrl: './UserAccount.component.html',
  styleUrls: ['./UserAccount.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserAccountComponent implements OnInit, FormInterface<User> {
  form = new FormGroup({});
  model = new User;
  properties = new UserProps;
  viewmodel = new UserViewModel;
  options: FormlyFormOptions = {};
  values: User[];


  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.fullName.toString(),
          templateOptions: {
            label: 'Full Name ',
          }
        },
        {
          type: 'input',
          key: this.properties.password.toString(),
          templateOptions: {
            label: 'Password',
            type: 'password'
          }
        },
        {
          type: 'input',
          key: this.properties.level.toString(),
          templateOptions: {
            label: 'Level',
          }
        },
        {
          type: 'input',
          key: this.properties.group.toString(),
          templateOptions: {
            label: 'Access Group',
          }
        },
        {
          type: 'input',
          key: this.properties.email.toString(),
          templateOptions: {
            label: 'E-mail',
          }
        }
      ],
    },
  ];

  constructor(private useraccountservice: UserService) { }

  ngOnInit() {
    this.useraccountservice.all
    .subscribe((data: User[]) => {
      this.values = data;
      console.log(data);
    });
  }

  submit() {
    this.useraccountservice.addData(this.model);
    this.model = new User;
  }
}
