import { FormInterface } from '../../../../inteface/form.interface';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Sample } from '../../../../models/sample.model';
import { DataService } from '../../../../services/data.service';
import { SampleService } from '../../../../services/sample.service';
import { SSS, SSSProps, SSSViewModel } from '../../../../models/sss.model';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { SSSService } from '../../../../services/sss.service';

@Component({
  selector: 'app-sss',
  templateUrl: './SSS.component.html',
  styleUrls: ['./SSS.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SSSComponent implements OnInit, FormInterface<SSS> {
  form = new FormGroup({});
  model = new SSS;
  properties = new SSSProps;
  viewmodel = new SSSViewModel;
  options: FormlyFormOptions = {};
  values: SSS[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          }
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description'
          }
        },
        {
          type: 'input',
          key: this.properties.MSC.toString(),
          templateOptions: {
            label: 'Monthly Salary Credit'
          }
        },
        {
          type: 'input',
          key: this.properties.rocStart.toString(),
          templateOptions: {
            label: 'Compensation Start'
          }
        },
        {
          type: 'input',
          key: this.properties.rocEnd.toString(),
          templateOptions: {
            label: 'Compensation End'
          }
        },
        {
          type: 'input',
          key: this.properties.SSER.toString(),
          templateOptions: {
            label: 'Employer Contribution'
          }
        },
        {
          type: 'input',
          key: this.properties.SSEE.toString(),
          templateOptions: {
            label: 'Employee Contribution'
          }
        },
        {
          type: 'input',
          key: this.properties.ECER.toString(),
          templateOptions: {
            label: 'ECOLA ER/EC'
          }
        }
      ]
    }
  ];

  constructor(private sssservice: SSSService) { }

  submit() {
    this.sssservice.addData(this.model);
    this.model = new SSS;
  }
  ngOnInit(): void {
    this.sssservice.all
    .subscribe((data: SSS[]) => {
      this.values = data;
      console.log(data);
    });
  }
}
