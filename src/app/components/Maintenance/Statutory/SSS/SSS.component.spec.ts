/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SSSComponent } from './SSS.component';

describe('SSSComponent', () => {
  let component: SSSComponent;
  let fixture: ComponentFixture<SSSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SSSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SSSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
