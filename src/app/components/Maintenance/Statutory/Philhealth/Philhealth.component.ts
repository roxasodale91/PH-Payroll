import { PhilHealth, PhilHealthViewModel, PhilHealthProps } from '../../../../models/philhealth.model';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../../inteface/form.interface';
import { FormGroup, NgModel } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { PhilHealthService } from '../../../../services/philhealth.service';

@Component({
  selector: 'app-philhealth',
  templateUrl: './Philhealth.component.html',
  styleUrls: ['./Philhealth.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PhilhealthComponent implements OnInit, FormInterface<PhilHealth> {
  viewmodel = new PhilHealthViewModel;
  properties = new PhilHealthProps;
  model = new PhilHealth;
  form = new FormGroup({});
  options: FormlyFormOptions = {};
  values: PhilHealth[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.rangeEnd.toString(),
          templateOptions: {
            label: 'Range End',
          }
        },
        {
          type: 'input',
          key: this.properties.ER.toString(),
          templateOptions: {
            label: 'ER',
          }
        },
        {
          type: 'input',
          key: this.properties.EE.toString(),
          templateOptions: {
            label: 'EE',
          }
        },
        {
          type: 'input',
          key: this.properties.percent.toString(),
          templateOptions: {
            label: 'Percent',
          }
        },
      ],
    },
  ];

  constructor(private philhealth: PhilHealthService) { }

  ngOnInit() {
    this.philhealth.all
    .subscribe((data: PhilHealth[]) => {
      this.values = data;
      console.log(data);
    });
  }

  submit() {
    this.philhealth.addData(this.model);
    this.model = new PhilHealth;
  }

}
