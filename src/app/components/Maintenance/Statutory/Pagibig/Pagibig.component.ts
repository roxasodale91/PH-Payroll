import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../../inteface/form.interface';
import { PagIbigProps, PagIbigViewModel, PagIbig } from '../../../../models/pagibig.model';
import { FormGroup, NgModel } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { PagIbigService } from '../../../../services/pagibig.service';

@Component({
  selector: 'app-pagibig',
  templateUrl: './Pagibig.component.html',
  styleUrls: ['./Pagibig.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PagibigComponent implements OnInit, FormInterface<PagIbig> {
  viewmodel = new PagIbigViewModel;
  properties = new PagIbigProps;
  model = new PagIbig;
  form = new FormGroup({});
  options: FormlyFormOptions = {};

  values: PagIbig[];
  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.rangeStart.toString(),
          templateOptions: {
            label: 'Range Start',
          }
        },
        {
          type: 'input',
          key: this.properties.rangeEnd.toString(),
          templateOptions: {
            label: 'Range End',
          }
        },
        {
          type: 'input',
          key: this.properties.ER.toString(),
          templateOptions: {
            label: 'Employer Contribution',
          }
        },
        {
          type: 'input',
          key: this.properties.EE.toString(),
          templateOptions: {
            label: 'Employee Contribution',
          }
        },
        {
          type: 'input',
          key: this.properties.total.toString(),
          templateOptions: {
            label: 'Total Contribution',
          }
        },
      ],
    },
  ];

  constructor(private pagibigservice: PagIbigService) { }

  ngOnInit() {
    this.pagibigservice.all
    .subscribe((data: PagIbig[]) => {
      this.values = data;
      console.log(data);
    });
  }

  submit() {
    this.pagibigservice.addData(this.model);
    this.model = new PagIbig;
  }
}
