/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PagibigComponent } from './Pagibig.component';

describe('PagibigComponent', () => {
  let component: PagibigComponent;
  let fixture: ComponentFixture<PagibigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagibigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagibigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
