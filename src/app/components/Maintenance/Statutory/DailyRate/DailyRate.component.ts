import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../../inteface/form.interface';
import { DailyRateProps, DailyRateViewmodel, DailyRate } from '../../../../models/dailyrate.model';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup, NgModel } from '@angular/forms';
import { DailyRateService } from '../../../../services/dailyrate.service';

@Component({
  selector: 'app-dailyrate',
  templateUrl: './DailyRate.component.html',
  styleUrls: ['./DailyRate.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DailyRateComponent implements OnInit, FormInterface<DailyRate> {
  form = new FormGroup({});
  model = new DailyRate;
  properties = new DailyRateProps;
  viewmodel = new DailyRateViewmodel;
  options: FormlyFormOptions = {};
  values: DailyRate[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.id.toString(),
          templateOptions: {
            label: 'ID'
          },
        },
        {
          type: 'input',
          key: this.properties.description.toString(),
          templateOptions: {
            label: 'Description ',
          }
        },
        {
          type: 'input',
          key: this.properties.BASC.toString(),
          templateOptions: {
            label: 'Basic',
          }
        },
        {
          type: 'input',
          key: this.properties.BSND.toString(),
          templateOptions: {
            label: 'Basic Night Diff',
          }
        },
        {
          type: 'input',
          key: this.properties.BSOT.toString(),
          templateOptions: {
            label: 'Basic OT',
          }
        },
        {
          type: 'input',
          key: this.properties.OTND.toString(),
          templateOptions: {
            label: 'OT Night Diff',
          }
        },
        {
          type: 'input',
          key: this.properties.FHRS.toString(),
          templateOptions: {
            label: 'Fixed Hour',
          }
        },
        {
          type: 'input',
          key: this.properties.FPLS.toString(),
          templateOptions: {
            label: 'Fixed Amount',
          }
        },
        {
          type: 'input',
          key: this.properties.SPLS.toString(),
          templateOptions: {
            label: 'Fixed Amount (Special)',
          }
        },
      ],
    },
  ];

  constructor(private dailyrateservice: DailyRateService) { }

  ngOnInit() {
    this.dailyrateservice.all
    .subscribe((data: DailyRate[]) => {
      this.values = data;
      console.log(data);
    });
  }

  submit() {
    this.dailyrateservice.addData(this.model);
    this.model = new DailyRate;
  }

}
