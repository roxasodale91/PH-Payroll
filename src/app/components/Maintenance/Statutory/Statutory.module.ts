import { NgModule } from '@angular/core';
import { StatutoryComponent } from './Statutory.component';
import { DailyRateComponent } from './DailyRate/DailyRate.component';
import { PagibigComponent } from './Pagibig/Pagibig.component';
import { PhilhealthComponent } from './Philhealth/Philhealth.component';
import { SSSComponent } from './SSS/SSS.component';
import { WithholdingComponent } from './Withholding/Withholding.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import { CommonModule } from '@angular/common';
import { SampleService } from '../../../services/sample.service';


@NgModule({
  imports: [
    FormlyModule,
    FormlyBootstrapModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    RouterModule,
  ],
  declarations:
  [
    // StatutoryComponent,
    // DailyRateComponent,
    // PagibigComponent,
    // PhilhealthComponent,
    // SSSComponent,
    // WithholdingComponent,
  ],
  providers: [SampleService]
})
export class StatutoryModule { }
