import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormInterface } from '../../../../inteface/form.interface';
import { WithHolding, WithHoldingProps, WithHoldingViewModel } from '../../../../models/withholding.model';
import { WithholdingService } from '../../../../services/withholding.service';


@Component({
  selector: 'app-withholding',
  templateUrl: './Withholding.component.html',
  styleUrls: ['./Withholding.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class WithholdingComponent implements OnInit, FormInterface<WithHolding> {
  form = new FormGroup({});
  model = new WithHolding;
  properties = new WithHoldingProps;
  viewmodel = new WithHoldingViewModel;
  options: FormlyFormOptions = {};
  values: WithHolding[];

  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'select',
          key: this.properties.taxType.toString(),
          templateOptions: {
            label: 'Tax Type',
            options: [
              {label: 'SEMI-MONTHLY', value: 'option1'},
              {label: 'MONTHLY', value: 'option2'},
              {label: 'QUARTERLY', value: 'option3'}
            ]
          },
        },
        {
          type: 'select',
          key: this.properties.exemptStatus.toString(),
          templateOptions: {
            label: 'Exemption Status',
            options: [
              {label: 'S/ME', value: 'option1'},
              {label: 'Option 2', value: 'option2'},
              {label: 'Option 3', value: 'option3'}
            ]
          },
        },
        {
          type: 'input',
          key: this.properties.exemptAmount.toString(),
          templateOptions: {
            label: 'Exemption Amount'
          },
        }
      ]
    },
    {
      template:
      '<div class="uk-grid uk-child-width-expand@m">' +
      '<label></label>' +
      '<label>Range Amount</label>' +
      '<label>Over</label>' +
      '<label>Percent%</label>' +
      '</div>'
    },
    {
      fieldGroupClassName: 'uk-grid uk-child-width-expand@m',
      fieldGroup: [
        {
          template: '<label>Table 1</label>'
        },
        {
          type: 'input',
          key: this.properties.amountOne.toString()
        },
        {
          type: 'input',
          key: this.properties.overOne.toString()
        },
        {
          type: 'input',
          key: this.properties.percentOne.toString()
        }
      ]
    }
  ];

  constructor(private withholdingservice: WithholdingService) {}

  ngOnInit() {
    this.withholdingservice.all
    .subscribe((data: WithHolding[]) => {
      this.values = data;
      console.log(data);
    });
  }

  submit() {
    this.withholdingservice.addData(this.model);
    this.model = new WithHolding;
  }

}
