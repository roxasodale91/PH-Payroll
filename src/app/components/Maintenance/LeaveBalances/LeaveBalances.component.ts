import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { Leave, LeaveProps, LeaveViewModel } from '../../../models/leave.model';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { LeaveBalancesService } from '../../../services/leavebalances.service';
import { TabType } from '../../../inteface/tabform.interface';

@Component({
  selector: 'app-leavebalances',
  templateUrl: './LeaveBalances.component.html',
  styleUrls: ['./LeaveBalances.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LeaveBalancesComponent implements OnInit, FormInterface<Leave> {
  fields: FormlyFieldConfig[];
  form = new FormGroup({});
  model = new Leave;
  properties = new LeaveProps;
  viewmodel = new LeaveViewModel;
  options: FormlyFormOptions = {};
  values: Leave[];

  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'input',
              key: this.properties.fullName.toString(),
              templateOptions: {
                label: 'Employee Name'
              },
            },
            {
              type: 'input',
              key: this.properties.companyId.toString(),
              templateOptions: {
                label: 'Company'
              },
            },
            {
              type: 'input',
              key: this.properties.outletName.toString(),
              templateOptions: {
                label: 'Outlet'
              },
            },
            {
              type: 'input',
              key: this.properties.dateHired.toString(),
              templateOptions: {
                label: 'Date Hired',
                type: 'date'
              },
            },
            {
              type: 'input',
              key: this.properties.genTotalHour.toString(),
              templateOptions: {
                label: 'Total Hours'
              },
            },
            {
              type: 'input',
              key: this.properties.genUsedHour.toString(),
              templateOptions: {
                label: 'Used Hour'
              },
            },
            {
              type: 'input',
              key: this.properties.genBalanceHour.toString(),
              templateOptions: {
                label: 'Balance Hour'
              },
            }
          ]
        }
      ]
    },
    {
      label: 'Leave Credit',
      fields: [
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'select',
              key: this.properties.leaveDesc.toString(),
              templateOptions: {
                label: 'Leave Description',
                options: [
                  {label: 'INCENTIVE LEAVE', value: 'option1'},
                  {label: 'VACATION LEAVE', value: 'option2'},
                  {label: 'SICK LEAVE', value: 'option3'}
                ]
              },
            },
            {
              type: 'input',
              key: this.properties.credTotalHour.toString(),
              templateOptions: {
                label: 'Total Hour'
              },
            },
            {
              type: 'input',
              key: this.properties.credUsedHour.toString(),
              templateOptions: {
                label: 'Used Hour'
              },
            },
            {
              type: 'input',
              key: this.properties.credBalanceHour.toString(),
              templateOptions: {
                label: 'Balance Hour'
              },
            },
            {
              type: 'input',
              key: this.properties.note.toString(),
              templateOptions: {
                label: 'Note'
              },
            },
          ]
        }
      ]
    }
  ];

  leaveCredit: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'select',
          key: this.properties.leaveDesc.toString(),
          templateOptions: {
            label: 'Leave Description',
            options: [
              {label: 'INCENTIVE LEAVE', value: 'option1'},
              {label: 'VACATION LEAVE', value: 'option2'},
              {label: 'SICK LEAVE', value: 'option3'}
            ]
          },
        },
        {
          type: 'input',
          key: this.properties.credTotalHour.toString(),
          templateOptions: {
            label: 'Total Hour'
          },
        },
        {
          type: 'input',
          key: this.properties.credUsedHour.toString(),
          templateOptions: {
            label: 'Used Hour'
          },
        },
        {
          type: 'input',
          key: this.properties.credBalanceHour.toString(),
          templateOptions: {
            label: 'Balance Hour'
          },
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note'
          },
        },
      ]
    }
  ];
  submit() {

    console.log(this.model);
    this.leavebalancesservice.addData(this.model);
    this.model = new Leave;
  }

  constructor(private leavebalancesservice: LeaveBalancesService) { }

  ngOnInit() {
    this.leavebalancesservice.all
    .subscribe((data: Leave[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
