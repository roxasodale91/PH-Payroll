import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { TimeKeeping, TimeKeepingProps, TimeKeepingViewModel } from '../../../models/timekeeping.model';
import { FormGroup } from '../../../../../node_modules/@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '../../../../../node_modules/@ngx-formly/core';
import { Employee } from '../../../models/employee.model';
import { Observable } from '../../../../../node_modules/rxjs';
import { TimeKeepingService } from '../../../services/timekeeping.service';
import { EmployeesService } from '../../../services/employees.service';
import { ExtensionService } from '../../../extensions/formatter.extension';

@Component({
  selector: 'app-timekeeping',
  templateUrl: './timekeeping.component.html',
  styleUrls: ['./timekeeping.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TimekeepingComponent implements OnInit, FormInterface<TimeKeeping> {
  form = new FormGroup({});
  model = new TimeKeeping;
  properties = new TimeKeepingProps;
  viewmodel = new TimeKeepingViewModel;
  options: FormlyFormOptions = {};
  values: TimeKeeping[];

  employees: Observable<Employee[]> = this.employeesservice.all;
  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.payPeriod.toString(),
          templateOptions: {
            label: 'Pay Period'
          }
        },
        {
          type: 'select',
          key: this.properties.employees.toString(),
          templateOptions: {
            label: 'Employee Name',
            options: this.extension.formatted
          }
        },
        {
          type: 'input',
          key: this.properties.companyId.toString(),
          templateOptions: {
            label: 'Company'
          }
        },
        {
          type: 'input',
          key: this.properties.outletName.toString(),
          templateOptions: {
            label: 'Outlet'
          }
        },
        {
          type: 'input',
          key: this.properties.txDate.toString(),
          templateOptions: {
            label: 'TxDate',
            type: 'date'
          }
        },
        {
          type: 'input',
          key: this.properties.regHour.toString(),
          templateOptions: {
            label: 'Regular Hour'
          }
        },
        {
          type: 'input',
          key: this.properties.otHour.toString(),
          templateOptions: {
            label: 'Overtime'
          }
        },
        {
          type: 'input',
          key: this.properties.otndHour.toString(),
          templateOptions: {
            label: 'Overtime Night Diff'
          }
        },
        {
          type: 'input',
          key: this.properties.ndHour.toString(),
          templateOptions: {
            label: 'Night Differential'
          }
        },
        {
          type: 'input',
          key: this.properties.absentHour.toString(),
          templateOptions: {
            label: 'Absent'
          }
        },
      ]
    }
  ];

  submit() {
    this.timekeepingservice.addData(this.model);
    this.model = new TimeKeeping;
  }

  constructor(private timekeepingservice: TimeKeepingService,
              private employeesservice: EmployeesService,
              private extension: ExtensionService) { }

  ngOnInit() {
    this.extension.format(this.employees);
    this.timekeepingservice.all
    .subscribe((data: TimeKeeping[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
