import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { Earning, EarningProps, EarningViewModel } from '../../../models/earning.model';
import { FormGroup } from '../../../../../node_modules/@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '../../../../../node_modules/@ngx-formly/core';
import { TabType } from '../../../inteface/tabform.interface';
import { Observable } from '../../../../../node_modules/rxjs';
import { Employee } from '../../../models/employee.model';
import { EarningsService } from '../../../services/earnings.service';
import { EmployeesService } from '../../../services/employees.service';
import { ExtensionService } from '../../../extensions/formatter.extension';
import { Template } from '../../../../../node_modules/@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-earnings',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EarningsComponent implements OnInit, FormInterface<Earning> {
  form = new FormGroup({});
  model = new Earning;
  properties = new EarningProps;
  viewmodel = new EarningViewModel;
  options: FormlyFormOptions = {};
  values: Earning[];
  fields: FormlyFieldConfig[];

  employees: Observable<Employee[]> = this.employeesservice.all;
  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          type: 'input',
          key: this.properties.payPeriod.toString(),
          templateOptions: {
            label: 'Pay Period'
          }
        },
        {
          type: 'select',
          key: this.properties.employees.toString(),
          templateOptions: {
            label: 'Employee Name',
            options: this.extension.formatted
          }
        },
        {
          type: 'input',
          key: this.properties.companyId.toString(),
          templateOptions: {
            label: 'Copmany'
          }
        },
        {
          type: 'input',
          key: this.properties.outletName.toString(),
          templateOptions: {
            label: 'Outlet'
          }
        },
        {
          type: 'input',
          key: this.properties.txDate.toString(),
          templateOptions: {
            label: 'Posting Date',
            type: 'date'
          }
        },
        {
          type: 'select',
          key: this.properties.earningType.toString(),
          templateOptions: {
            label: 'Earning Type',
            options: [
              {label: '1', value: 'option1'},
              {label: '2', value: 'option2'}
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.earningDescription.toString(),
          templateOptions: {
            label: 'Description',
            options: [
              {label: 'Allowance', value: 'option1'},
              {label: 'Technical Allowance', value: 'option2'}
            ]
          }
        },
        {
          type: 'select',
          key: this.properties.scheduleMode.toString(),
          templateOptions: {
            label: 'Schedule',
            options: [
              {label: 'Semi', value: 'option1'},
              {label: 'Middle', value: 'option2'}
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.amount.toString(),
          templateOptions: {
            label: 'Amount'
          }
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note'
          }
        },
      ]
    },
    {
      label: 'Scheduled Earning List',
      fields: [
        // Template:
      ]
    }
  ];

  submit() {
    this.earningsservice.addData(this.model);
    this.model = new Earning;
  }

  constructor(private earningsservice: EarningsService,
              private employeesservice: EmployeesService,
              private extension: ExtensionService) { }

  ngOnInit() {
    this.extension.format(this.employees);
    this.earningsservice.all
    .subscribe((data: Earning[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
