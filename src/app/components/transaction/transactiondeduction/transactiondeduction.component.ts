import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { TransactionDeduction, TransactionDeductionProps, TransactionDeductionViewModel } from '../../../models/transactiondeduction.model';
import { FormGroup } from '../../../../../node_modules/@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '../../../../../node_modules/@ngx-formly/core';
import { TabType } from '../../../inteface/tabform.interface';
import { TransactionDeductionService } from '../../../services/transactiondeduction.service';
import { EmployeesService } from '../../../services/employees.service';
import { ExtensionService } from '../../../extensions/formatter.extension';
import { Observable } from '../../../../../node_modules/rxjs';
import { Employee } from '../../../models/employee.model';

@Component({
  selector: 'app-transactiondeduction',
  templateUrl: './transactiondeduction.component.html',
  styleUrls: ['./transactiondeduction.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TransactiondeductionComponent implements OnInit, FormInterface<TransactionDeduction> {
  form = new FormGroup({});
  model = new TransactionDeduction;
  properties = new TransactionDeductionProps;
  viewmodel = new TransactionDeductionViewModel;
  options: FormlyFormOptions = {};
  values: TransactionDeduction[];
  fields: FormlyFieldConfig[];

  employees: Observable<Employee[]> = this.employeesservice.all;
  tabs: TabType[] = [
    {
      label: 'General',
      fields: [
        {
          fieldGroup: [
            {
              type: 'input',
              key: this.properties.payPeriod.toString(),
              templateOptions: {
                label: 'Pay Period'
              }
            },
            {
              type: 'select',
              key: this.properties.employees.toString(),
              templateOptions: {
                label: 'Employee Name',
                options: this.extension.formatted
              }
            },
            {
              type: 'input',
              key: this.properties.companyId.toString(),
              templateOptions: {
                label: 'Company'
              }
            },
            {
              type: 'select',
              key: this.properties.outletName.toString(),
              templateOptions: {
                label: 'Outlet',
                options: [
                  {label: 'SHELL OPTIMUS', value: 'option1'},
                  {label: 'RACKS TIMOG', value: 'option2'}
                ]
              }
            },
          ]
        },
        {
          fieldGroupClassName: 'uk-grid uk-child-width-1-2@m',
          fieldGroup: [
            {
              type: 'input',
              key: this.properties.txDate.toString(),
              templateOptions: {
                label: 'Posting Date',
                type: 'date'
              }
            },
            {
              type: 'input',
              key: this.properties.loanDate.toString(),
              templateOptions: {
                label: 'Loan Date',
                type: 'date'
              }
            },
            {
              type: 'select',
              key: this.properties.typeDescription.toString(),
              templateOptions: {
                label: 'Deduction Type',
                options: [
                  {label: 'REGULAR DEDUCTION', value: 'option1'},
                  {label: 'STATUTORIES', value: 'option2'}
                ]
              }
            },
            {
              type: 'select',
              key: this.properties.deductionDescription.toString(),
              templateOptions: {
                label: 'Deduction Description',
                options: [
                  {label: 'SSS LOAN', value: 'option1'},
                  {label: 'SM CREDIT CARD', value: 'option2'}
                ]
              }
            },
          ]
        },
        {
          fieldGroup: [
            {
              type: 'select',
              key: this.properties.scheduleMode.toString(),
              templateOptions: {
                label: 'Schedule Mode',
                options: [
                  {label: 'MIDDLE', value: 'option1'},
                  {label: 'STOP', value: 'option2'},
                  {label: 'SEMI', value: 'option3'}
                ]
              }
            },
            {
              type: 'input',
              key: this.properties.amount.toString(),
              templateOptions: {
                label: 'Amount',
                type: 'number'
              }
            },
            {
              type: 'input',
              key: this.properties.note.toString(),
              templateOptions: {
                label: 'Note'
              }
            },
          ]
        }
      ],
    },
  ];

  submit() {
    this.transactiondeductionservice.addData(this.model);
    this.model = new TransactionDeduction;
  }

  constructor(private transactiondeductionservice: TransactionDeductionService,
              private employeesservice: EmployeesService,
              private extension: ExtensionService) { }

  ngOnInit() {
    this.extension.format(this.employees);
    this.transactiondeductionservice.all
    .subscribe((data: TransactionDeduction[]) => {
      this.values = data;
      console.log(data);
     });
  }

}
