import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimekeepingComponent } from './timekeeping/timekeeping.component';
import { ProcesspayrollComponent } from './processpayroll/processpayroll.component';
import { EarningsComponent } from './earnings/earnings.component';
import { DailyleaveComponent } from './dailyleave/dailyleave.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionComponent } from './transaction.component';
import { RouterModule } from '@angular/router';
import { FormlyModule } from '@ngx-formly/core';
import { TransactiondeductionComponent } from './transactiondeduction/transactiondeduction.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormlyModule,
    FormlyBootstrapModule,
    FormsModule,
    CommonModule

  ],
  declarations: [
]
})
export class TransactionModule { }
