import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormInterface } from '../../../inteface/form.interface';
import { DailyLeave, DailyLeaveProps, DailyLeaveViewModel } from '../../../models/dailyleave.model';
import { FormGroup } from '../../../../../node_modules/@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '../../../../../node_modules/@ngx-formly/core';
import { EmployeesService } from '../../../services/employees.service';
import { ExtensionService } from '../../../extensions/formatter.extension';
import { Observable } from '../../../../../node_modules/rxjs';
import { Employee } from '../../../models/employee.model';
import { DailyLeaveService } from '../../../services/dailyleave.service';

@Component({
  selector: 'app-dailyleave',
  templateUrl: './dailyleave.component.html',
  styleUrls: ['./dailyleave.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DailyleaveComponent implements OnInit, FormInterface<DailyLeave> {
  form = new FormGroup({});
  model = new DailyLeave;
  properties = new DailyLeaveProps;
  viewmodel = new DailyLeaveViewModel;
  options: FormlyFormOptions = {};
  values: DailyLeave[];

  employees: Observable<Employee[]> = this.employeesservice.all;
  fields: FormlyFieldConfig[] = [
    {
      fieldGroup: [
        {
          type: 'input',
          key: this.properties.payPeriod.toString(),
          templateOptions: {
            label: 'Pay Period'
          },
        },
        {
          type: 'select',
          key: this.properties.employees.toString(),
          templateOptions: {
            label: 'Employee Name',
            options: this.extension.formatted
          }
        },
        {
          type: 'input',
          key: this.properties.companyId.toString(),
          templateOptions: {
            label: 'Company'
          }
        },
        {
          type: 'input',
          key: this.properties.outletName.toString(),
          templateOptions: {
            label: 'Outlet'
          }
        },
      ]
    },
    {
      fieldGroupClassName: 'uk-grid',
      fieldGroup: [
        {
          template: '<label class="uk-width-auto">Availed Date</label>'
        },
        {
          className: 'uk-width-expand@m',
          type: 'input',
          key: this.properties.txDateStart.toString(),
          templateOptions: {
            type: 'date'
          }
        },
        {
          template: '<label class="uk-width-auto">To</label>'
        },
        {
          className: 'uk-width-expand@m',
          type: 'input',
          key: this.properties.txDateEnd.toString(),
          templateOptions: {
            type: 'date'
          }
        },
      ]
    },
    {
      fieldGroup: [
        {
          type: 'select',
          key: this.properties.leaveDesc.toString(),
          templateOptions: {
            label: 'Leave Type',
            options: [
              {label: 'INCENTIVE LEAVE', value: 'option1'},
              {label: 'VACATION LEAVE', value: 'option2'}
            ]
          }
        },
        {
          type: 'input',
          key: this.properties.totalHour.toString(),
          templateOptions: {
            label: 'Available Hour',
            type: 'disable'
          }
        },
        {
          type: 'input',
          key: this.properties.usedHour.toString(),
          templateOptions: {
            label: 'Used Hour'
          }
        },
        {
          type: 'input',
          key: this.properties.note.toString(),
          templateOptions: {
            label: 'Note'
          }
        },
      ]
    }
  ];

  submit() {
    this.dailyleaveservice.addData(this.model);
    this.model = new DailyLeave;
  }

  constructor(private dailyleaveservice: DailyLeaveService,
              private employeesservice: EmployeesService,
              private extension: ExtensionService) { }

  ngOnInit() {
    this.extension.format(this.employees);
    this.dailyleaveservice.all
    .subscribe((data: DailyLeave[]) => {
      this.values = data;
      console.log(data);
    });
  }

}
