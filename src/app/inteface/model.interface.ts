export interface ViewModelInterface<T> {

  object: T;

  add(object: T);
  update(object: T);
  delete(id: string);
  getAll();
  get(id: string);
}
