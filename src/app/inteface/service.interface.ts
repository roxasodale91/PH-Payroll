import { HttpClient } from '@angular/common/http';
import { CompanyArea } from '../models/area.model';
import { BehaviorSubject, Observable, observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

export interface ServiceInterface<T> {
  dataSubject: BehaviorSubject<T[]>;
  apiURL: string;
  dataStore: {
    data: T[];
  };
  /**
   *Implement as 'get subjectObservable() { return this.dataSubject.asObservable(); }'
   *
   * @memberof ServiceInterface
   */
  subjectObservable();

  /**
   * Use this method as this service's initializer
   *loadAll() { this.http.get<T[]>(this.apiURL)
    .subscribe(data => { this.dataStore.data = data;
      this.dataSubject.next(Object.assign({}, this.dataStore).data);
    });
  }
   *
   * @memberof ServiceInterface
   */
  loadAll();
}
