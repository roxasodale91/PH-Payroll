import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyConfig, FormlyFieldConfig } from '@ngx-formly/core';

export interface FormInterface<T> {
    /**
     * This is the form group that will be used in the Component.html
     * ex. form = new FormGroup({});
     */
    form: FormGroup;
    /**
     * Define here the Model <T> that you want to cast your form values to
     * ex. model = new CompanyArea;
     */
    model: T;
    /**
     * Define here the ModelProps <T> that you want to cast your form values to
     * ex. properties = new CompanyAreaProps;
     */
    properties: any;
    /**
     * Define here the ModelViewModel <T> that you want to cast your form values to
     * ex. viewmodel = new CompanyAreaViewModel;
     */
    viewmodel: any;
    /**
     * options = {};
     */
    options: FormlyFormOptions;
    /**
     * Use this to store values from an existing list ( API Service, List, etc.)
     * use this to bind data to the Component.html
     * ex. values = this.viewmodel.getAll();
     */
    values: T[];
    /**
     * Forms that you will use for your Component
     */
    fields: FormlyFieldConfig[];

    submit();

    // take(id: string);
}
