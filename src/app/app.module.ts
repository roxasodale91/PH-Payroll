import { ModalComponent } from './directives/modal/modal.component';
import { HomeComponent } from './components/home/home.component';
import { TransactionModule } from './components/transaction/transaction.module';
import { ExtensionService } from './extensions/formatter.extension';
import { AreaService } from './services/area.service';
import { ProfileService } from './services/profile.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import { CompanyComponent } from './components/company/company.component';
import { AreaComponent } from './components/company/area/area.component';
import {routes, appRoutes} from './app.routing';
import { ResizeEvent, ResizableModule } from 'angular-resizable-element';
import { AppComponent } from './app.component';
import { MaintenanceModule } from './components/Maintenance/Maintenance.module';
import { StatutoryModule } from './components/Maintenance/Statutory/Statutory.module';
import { PanelWrapperComponent } from './components/wrappers/custom-wrapper.component';
import { EmployeesComponent } from './components/company/employees/employees.component';
import { OutletComponent } from './components/company/outlet/outlet.component';
import { DepartmentComponent } from './components/company/department/department.component';
import { DivisionComponent } from './components/company/division/division.component';
import { PositionComponent } from './components/company/position/position.component';
import { SalarytableComponent } from './components/company/salarytable/salarytable.component';
import { LockunlockpayperiodComponent } from './components/company/lockunlockpayperiod/lockunlockpayperiod.component';
import { ProfileComponent } from './components/company/profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { DepartmentService } from './services/department.service';
import { DailyRateService } from './services/dailyrate.service';
import { PagIbigService } from './services/pagibig.service';
import { PhilHealthService } from './services/philhealth.service';
import { SSSService } from './services/sss.service';
import { UserService } from './services/user.service';
import { WithholdingService } from './services/withholding.service';
import { HolidaySetupService } from './services/holiday.service';
import { LeaveBalancesService } from './services/leavebalances.service';
import { DeductionService } from './services/deduction.service';
import { DivisionService } from './services/division.service';
import { EmployeesService } from './services/employees.service';
import { LockUnlockService } from './services/lockunlock.service';
import { OutletService } from './services/outlet.service';
import { PositionService } from './services/position.service';
import { SalaryTableService } from './services/salarytable.service';
import { UseraccessComponent } from './components/useraccess/useraccess.component';
import { DeductionComponent } from './components/Maintenance/Deduction/Deduction.component';
import { EarningsService } from './services/earnings.service';
import { ModalService } from './services/modal.service';
import { DailyLeaveService } from './services/dailyleave.service';
import { TimeKeepingService } from './services/timekeeping.service';
import { TransactionDeductionService } from './services/transactiondeduction.service';
import { TransactionComponent } from './components/transaction/transaction.component';
import { DailyleaveComponent } from './components/transaction/dailyleave/dailyleave.component';
import { EarningsComponent } from './components/transaction/earnings/earnings.component';
import { ProcesspayrollComponent } from './components/transaction/processpayroll/processpayroll.component';
import { TimekeepingComponent } from './components/transaction/timekeeping/timekeeping.component';
import { TransactiondeductionComponent } from './components/transaction/transactiondeduction/transactiondeduction.component';
import { MaintenanceComponent } from './components/Maintenance/Maintenance.component';
import { HolidaySetupComponent } from './components/Maintenance/HolidaySetup/HolidaySetup.component';
import { LeaveBalancesComponent } from './components/Maintenance/LeaveBalances/LeaveBalances.component';
import { StatutoryComponent } from './components/Maintenance/Statutory/Statutory.component';
import { UserAccountComponent } from './components/Maintenance/UserAccount/UserAccount.component';
import { DailyScheduleComponent } from './components/Maintenance/DailySchedule/DailySchedule.component';
import { DailyRateComponent } from './components/Maintenance/Statutory/DailyRate/DailyRate.component';
import { PagibigComponent } from './components/Maintenance/Statutory/Pagibig/Pagibig.component';
import { PhilhealthComponent } from './components/Maintenance/Statutory/Philhealth/Philhealth.component';
import { SSSComponent } from './components/Maintenance/Statutory/SSS/SSS.component';
import { WithholdingComponent } from './components/Maintenance/Statutory/Withholding/Withholding.component';

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    AreaComponent,
    PanelWrapperComponent,
    EmployeesComponent,
    OutletComponent,
    DepartmentComponent,
    DivisionComponent,
    PositionComponent,
    SalarytableComponent,
    LockunlockpayperiodComponent,
    ProfileComponent,
    HomeComponent,
    UseraccessComponent,
    ModalComponent,
    TransactionComponent,
    DailyleaveComponent,
    EarningsComponent,
    ProcesspayrollComponent,
    TimekeepingComponent,
    TransactiondeductionComponent,
    MaintenanceComponent,
    DeductionComponent,
    HolidaySetupComponent,
    LeaveBalancesComponent,
    StatutoryComponent,
    UserAccountComponent,
    DailyScheduleComponent,
    StatutoryComponent,
    DailyRateComponent,
    PagibigComponent,
    PhilhealthComponent,
    SSSComponent,
    WithholdingComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyModule.forRoot(
      {
        wrappers: [
          { name: 'panel', component: PanelWrapperComponent },
        ],
        // types: [
        //   { name: 'repeat', component: repeatTypeComponent }
        // ]
      }
    ),
    FormlyBootstrapModule,
    routes,
    appRoutes,
    // MaintenanceModule,
    StatutoryModule,
    HttpClientModule,
    TransactionModule,
    ResizableModule
  ],
  providers: [
    ProfileService,
    AreaService,
    ExtensionService,
    DepartmentService,
    DailyRateService,
    PagIbigService,
    PhilHealthService,
    SSSService,
    UserService,
    WithholdingService,
    HolidaySetupService,
    LeaveBalancesService,
    DeductionService,
    DivisionService,
    EmployeesService,
    LockUnlockService,
    OutletService,
    PositionService,
    ProfileService,
    SalaryTableService,
    EarningsService,
    ModalService,
    DailyLeaveService,
    TimeKeepingService,
    TransactionDeductionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
