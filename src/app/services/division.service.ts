import { HttpService } from '../inteface/httpservice.interface';
import { CompanyDivision } from '../models/division.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DivisionService extends HttpService<CompanyDivision> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
