import { HttpService } from '../inteface/httpservice.interface';
import { Earning } from '../models/earning.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '../../../node_modules/@angular/core';

@Injectable()
export class EarningsService extends HttpService<Earning> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/earnings/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
