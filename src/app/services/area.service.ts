import { HttpService } from '../inteface/httpservice.interface';
import { HttpClient } from '@angular/common/http';
import { CompanyArea } from '../models/area.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AreaService extends HttpService<CompanyArea> {

  mockData: CompanyArea[];

  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/All';
  }
}

