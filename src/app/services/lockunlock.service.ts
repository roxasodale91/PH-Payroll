import { Injectable } from '@angular/core';
import { HttpService } from '../inteface/httpservice.interface';
import { LockUnlock } from '../models/lockunlock.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LockUnlockService extends HttpService<LockUnlock> {
  static all: any;
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
