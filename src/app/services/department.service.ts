import { HttpService } from '../inteface/httpservice.interface';
import { HttpClient } from '@angular/common/http';
import { CompanyDepartment } from '../models/department.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DepartmentService extends HttpService<CompanyDepartment> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/departments/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
