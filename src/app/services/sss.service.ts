import { Injectable } from '@angular/core';
import { HttpService } from '../inteface/httpservice.interface';
import { SSS } from '../models/sss.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SSSService extends HttpService<SSS> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
