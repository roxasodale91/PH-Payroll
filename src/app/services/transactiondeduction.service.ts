import { Injectable } from '../../../node_modules/@angular/core';
import { HttpService } from '../inteface/httpservice.interface';
import { TransactionDeduction } from '../models/transactiondeduction.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TransactionDeductionService extends HttpService<TransactionDeduction> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
