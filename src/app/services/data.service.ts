import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs';

export interface DataService<T> {

  /**
   * API Calls should assign values here
   */
  dataArray: T[];
  /**
   * Implement the values of the dataarray here
   *  behaviorSubject: BehaviorSubject<T[]>(this.dataArray);
   */
  behaviorSubject: BehaviorSubject<T[]>;

  /**
   * This is the accessed Data for this Service
   * ex. data = this.behaviorSubject.asObservable();
   */
  data: any;

  /**
   * Call API Service here
   * ex. apiCallMethod('Area')
   * this.dataArray = api call method
   * @param Component Name
   */
  getService(component: string);

  /**
   * Post API Call
   * Use this method to throw a single record onto the API
   */
  postService(data: T);
  /**
   * Put API Call
   * Use this method to update a single record then throw onto the API
   */
  putService(data: T);
   /**
   * Delete API Call
   * Use this method to remove a single record onto the API
   */
  deleteService(id: string);

  /**
   * You are changing the current values of the list here
   * this.behaviorSubject.next(newData);
   * Use this only to just cache lists
   * implement another function to throw new/updated entries to API
   * @param newData Sample[]
   */
  changeService(newData: T[]);

}
