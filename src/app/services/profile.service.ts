import { HttpService } from '../inteface/httpservice.interface';
import { Company } from '../models/company.model';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class ProfileService extends HttpService<Company> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/companies/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
