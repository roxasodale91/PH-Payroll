import { Employee } from '../models/employee.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../inteface/httpservice.interface';

@Injectable()
export class EmployeesService extends HttpService<Employee> {
  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'http://api.phpayroll.net/areas/2FF74D84-B151-4B5F-B476-2968DEDAD3E6/';
  }
}
