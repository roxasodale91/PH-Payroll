import { ViewModelInterface } from '../inteface/model.interface';

export class DailyLeave {
  payPeriod: string;
  txDateStart: string | Date;
  txDateEnd: string | Date;
  empId: string | number;
  fullName: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  leaveId: string | number;
  leaveDesc: string;
  totalHour: string | number;
  usedHour: string | number;
  balanceHour: string | number;
  note: string;
  employees: string;
}

export class DailyLeaveProps implements DailyLeave {
  payPeriod: string;
  txDateStart: string | Date;
  txDateEnd: string | Date;
  empId: string | number;
  fullName: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  leaveId: string | number;
  leaveDesc: string;
  totalHour: string | number;
  usedHour: string | number;
  balanceHour: string | number;
  note: string;
  employees: string;

  constructor() {
    this.payPeriod = 'payPeriod';
    this.txDateStart = 'txDateStart';
    this.txDateEnd = 'txDateEnd';
    this.empId = 'empId';
    this.fullName = 'fullName';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.leaveId = 'leaveId';
    this.leaveDesc = 'leaveDesc';
    this.totalHour = 'totalHour';
    this.usedHour = 'usedHour';
    this.balanceHour = 'balanceHour';
    this.note = 'note';
    this.employees = 'employees';
  }
}

export class DailyLeaveViewModel implements ViewModelInterface<DailyLeave> {
  object: DailyLeave;  add(object: DailyLeave) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyLeave) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
