import { ViewModelInterface } from '../inteface/model.interface';

export class TimeKeeping {
  empId: string | number;
  fullName: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  payPeriod: string;
  txDate: string | Date;
  regHour: string | number;
  otHour: string | number;
  otndHour: string | number;
  ndHour: string | number;
  absentHour: string | number;
  totalAmount: string | number;
  employees: string;
}

export class TimeKeepingProps implements TimeKeeping {
  empId: string | number;
  fullName: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  payPeriod: string;
  txDate: string | Date;
  regHour: string | number;
  otHour: string | number;
  otndHour: string | number;
  ndHour: string | number;
  absentHour: string | number;
  totalAmount: string | number;
  employees: string;

  constructor() {
    this.empId = 'empId';
    this.fullName = 'fullName';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.payPeriod = 'payPeriod';
    this.txDate = 'txDate';
    this.regHour = 'regHour';
    this.otHour = 'otHour';
    this.otndHour = 'otndHour';
    this.ndHour = 'ndHour';
    this.absentHour = 'absentHour';
    this.totalAmount = 'totalAmount';
    this.employees = 'employees';
  }
}

export class TimeKeepingViewModel implements ViewModelInterface<TimeKeeping> {
  object: TimeKeeping;  add(object: TimeKeeping) {
    throw new Error('Method not implemented.');
  }
  update(object: TimeKeeping) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
