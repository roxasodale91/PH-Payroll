﻿import { ViewModelInterface } from '../inteface/model.interface';
export class CompanyDivision {
  id: number | string;
  description: string;
  note: string;
  levelId: number | string;
  salaryId: number | string;
}

export class CompanyDivisionProps implements CompanyDivision {
  id: string | number;  description: string;
  note: string;
  levelId: number | string;
  salaryId: number | string;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.note = 'note';
    this.levelId = 'levelId';
    this.salaryId = 'salaryId';

  }
}
export class CompanyDivisionViewModel implements ViewModelInterface<CompanyDivision > {
  object: CompanyDivision;

  add(object: CompanyDivision) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanyDivision) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
