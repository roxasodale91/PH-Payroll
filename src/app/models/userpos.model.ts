import { ViewModelInterface } from '../inteface/model.interface';
import { UserRolePOS } from './userolepos.model';
import { UserAccessType } from './useraccesstype.model';

export class UserPOS {
  userID: number | string;
  UserName: string;
  FoodServerid: number | string;
  roleId: number | string;
  email: string;
  mobile: string;
  lastLogin: Date | string;
  picture: string;
  company: string;
  outlet: number | string;
  dailySchedule: number | string;
  timeIn: boolean | string;
  staticUser: boolean | string;

  userRolePOS: UserRolePOS;
  userAccessType: UserAccessType[];
}

export class UserPOSProps implements UserPOS {
  userID: string | number;  UserName: string;
  FoodServerid: string | number;
  roleId: string | number;
  email: string;
  mobile: string;
  lastLogin: string | Date;
  picture: string;
  company: string;
  outlet: string | number;
  dailySchedule: string | number;
  timeIn: string | boolean;
  staticUser: string | boolean;
  userRolePOS: UserRolePOS;
  userAccessType: UserAccessType[];

  constructor() {
    this.userID = 'userID';
    this.UserName = 'UserName';
    this.FoodServerid = 'FoodServerid';
    this.roleId = 'roleId';
    this.email = 'email';
    this.mobile = 'mobile';
    this.lastLogin = 'lastLogin';
    this.picture = 'picture';
    this.company = 'company';
    this.outlet = 'outlet';
    this.dailySchedule = 'dailySchedule';
    this.timeIn = 'timeIn';
    this.staticUser = 'staticUser';

  }
}

export class UserPOSViewModel implements ViewModelInterface<UserPOS> {
  object: UserPOS;

  add(object: UserPOS) {
    throw new Error('Method not implemented.');
  }
  update(object: UserPOS) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}

