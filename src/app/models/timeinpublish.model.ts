﻿import { ViewModelInterface } from '../inteface/model.interface';

export class DailyTimeInPublish {
  companyId: string;
  startDate: Date | string;
  endDate: Date | string;
}

export class DailyTimeInPublishProps implements DailyTimeInPublish {
  companyId: string;  startDate: string | Date;
  endDate: string | Date;

  constructor() {
    this.companyId = 'companyId';
    this.startDate = 'startDate';
    this.endDate = 'endDate';

  }
}
export class DailyTimeInPublishViewModel implements ViewModelInterface<DailyTimeInPublish> {
  object: DailyTimeInPublish;  add(object: DailyTimeInPublish) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyTimeInPublish) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
