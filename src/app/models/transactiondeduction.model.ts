import { ViewModelInterface } from '../inteface/model.interface';

export class TransactionDeduction {
  fullName: string;
  empId: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  txDate: Date | string;
  loanDate: Date | string;
  deductionDescription: string;
  typeDescription: string;
  scheduleMode: string;
  employees: string;
  amount: number | string;
  note: string;
}

export class TransactionDeductionProps implements TransactionDeduction {
  fullName: string;  empId: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  payPeriod: string;
  txDate: string | Date;
  loanDate: string | Date;
  deductionDescription: string;
  typeDescription: string;
  scheduleMode: string;
  employees: string;
  amount: number | string;
  note: string;

  constructor() {
    this.fullName = 'fullName';
    this.empId = 'empId';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.payPeriod = 'payPeriod';
    this.txDate = 'txDate';
    this.loanDate = 'loanDate';
    this.deductionDescription = 'deductionDescription';
    this.typeDescription = 'typeDescription';
    this.scheduleMode = 'scheduleMode';
    this.employees = 'employees';
    this.amount = 'amount';
    this.note = 'note';
  }
}

export class TransactionDeductionViewModel implements ViewModelInterface<TransactionDeduction> {
  object: TransactionDeduction;  add(object: TransactionDeduction) {
    throw new Error('Method not implemented.');
  }
  update(object: TransactionDeduction) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
