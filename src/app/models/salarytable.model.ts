﻿import { ViewModelInterface } from '../inteface/model.interface';
export class CompanySalaryTable {
  id: string;
  description: string;
  note: string;
  rateMonthly: number | string;
  rateDaily: number | string;
  rateHourly: number | string;
}

export class CompanySalaryTableProps implements CompanySalaryTable {
  id: string;  description: string;
  note: string;
  rateMonthly: number | string;
  rateDaily: number | string;
  rateHourly: number | string;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.note = 'note';
    this.rateMonthly = 'rateMonthly';
    this.rateDaily = 'rateDaily';
    this.rateHourly = 'rateHourly';

  }
}
export class CompanySalaryTableViewModel implements ViewModelInterface<CompanySalaryTable> {
  object: CompanySalaryTable;

  add(object: CompanySalaryTable) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanySalaryTable) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
