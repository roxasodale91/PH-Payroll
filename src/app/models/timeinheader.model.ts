﻿import { ViewModelInterface } from '../inteface/model.interface';
export class DailyTimeInHeader {
  empId: string;
  fullName: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  Reg_Hour: number | string;
  OT_Hour: number | string;
  OTND_Hour: number | string;
  ND_Hour: number | string;
  Absent_Hour: number | string;
  totalAmount: number | string;
}

export class DailyTimeInHeaderProps implements DailyTimeInHeader {
  empId: string;  fullName: string;
  companyId: string;
  outletId: string | number;
  outletName: string;
  payPeriod: string;
  Reg_Hour: string | number;
  OT_Hour: string | number;
  OTND_Hour: string | number;
  ND_Hour: string | number;
  Absent_Hour: string | number;
  totalAmount: string | number;

  constructor() {
    this.empId = 'empId';
    this.fullName = 'fullName';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.payPeriod = 'payPeriod';
    this.Reg_Hour = 'Reg_Hour';
    this.OT_Hour = 'OT_Hour';
    this.OTND_Hour = 'OTND_Hour';
    this.ND_Hour = 'ND_Hour';
    this.Absent_Hour = 'Absent_Hour';
    this.totalAmount = 'totalAmount';

  }

}

export class DailyTimeInHeaderViewModel implements ViewModelInterface<DailyTimeInHeader> {
  object: DailyTimeInHeader;

  add(object: DailyTimeInHeader) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyTimeInHeader) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
