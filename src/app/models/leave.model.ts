﻿import { ViewModelInterface } from '../inteface/model.interface';

export class Leave {
  empId: string;
  fullName: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  dateHired: Date | string;
  genTotalHour: number | string;
  genUsedHour: number | string;
  genBalanceHour: number | string;
  updateBy: string;
  updateDate: Date | string;
  leaveDesc: string;
  credTotalHour: number | string;
  credUsedHour: number | string;
  credBalanceHour: number | string;
  note: string;
}

export class LeaveProps implements Leave {
  empId: string;
  fullName: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  dateHired: Date | string;
  genTotalHour: number | string;
  genUsedHour: number | string;
  genBalanceHour: number | string;
  updateBy: string;
  updateDate: Date | string;
  leaveDesc: string;
  credTotalHour: number | string;
  credUsedHour: number | string;
  credBalanceHour: number | string;
  note: string;

  constructor() {
    this.empId = 'empId';
    this.fullName = 'fullName';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.dateHired = 'dateHired';
    this.genTotalHour = 'genTotalHour';
    this.genUsedHour = 'genUsedHour';
    this.genBalanceHour = 'genBalanceHour';
    this.updateBy = 'updateBy';
    this.updateDate = 'updateDate';
    this.credTotalHour = 'credTotalHour';
    this.credUsedHour = 'credUsedHour';
    this.credBalanceHour = 'credBalanceHour';
    this.leaveDesc = 'leaveDesc';
    this.note = 'note';

  }
}
export class LeaveViewModel implements ViewModelInterface<Leave> {
  object: Leave;

  add(object: Leave) {
    throw new Error('Method not implemented.');
  }
  update(object: Leave) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
