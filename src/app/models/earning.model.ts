﻿import { ViewModelInterface } from '../inteface/model.interface';
export class Earning {
  fullName: string;
  empId: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  txDate: Date | string;
  amount: number | string;
  earningId: number | string;
  earningDescription: string;
  earningType: number | string;
  typeDescription: string;
  scheduleMode: string;
  note: string;
  updateBy: string;
  updateDate: Date | string;
  locked: boolean | string;
  employees: string;
}

export class EarningProps implements Earning {
  fullName: string;
  empId: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  txDate: Date | string;
  amount: number | string;
  earningId: number | string;
  earningDescription: string;
  earningType: number | string;
  typeDescription: string;
  scheduleMode: string;
  note: string;
  updateBy: string;
  updateDate: Date | string;
  locked: boolean | string;
  employees: string;

  constructor() {
    this.fullName = 'fullName';
    this.empId = 'empId';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.outletName = 'outletName';
    this.payPeriod = 'payPeriod';
    this.txDate = 'txDate';
    this.amount = 'amount';
    this.earningId = 'earningId';
    this.earningDescription = 'earningDescription';
    this.earningType = 'earningType';
    this.earningDescription = 'earningDescription';
    this.scheduleMode = 'scheduleMode';
    this.note = 'note';
    this.updateBy = 'updateBy';
    this.updateDate = 'updateDate';
    this.locked = 'locked';
    this.employees = 'employees';
  }
}

export class EarningViewModel implements ViewModelInterface<Earning> {
  object: Earning;

  add(object: Earning) {
    throw new Error('Method not implemented.');
  }
  update(object: Earning) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
