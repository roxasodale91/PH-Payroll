import { ViewModelInterface } from '../inteface/model.interface';
export class Employee {
  empId: string;
  prevId: string;
  password: string;
  lastName: string;
  firstName: string;
  middleName: string;
  suffixName: string;
  birthday: Date | string;
  gender: string;
  civilStatus: string;
  company: string;
  outlet: number | string;
  area: number | string;
  department: number | string;
  division: number | string;
  position: number | string;
  payPeriod: string;
  billingRate: string;
  workSchedule: string;
  rateTable: string;
  rateMonthly: number | string;
  rateDaily: number | string;
  rateHourly: number | string;
  empStatus: string;
  dateHired: Date | string;
  dateRegularized: Date | string;
  dateResigned: Date | string;
  remarks: string;
  bankAccountNumber: string;
  holdBankProcess: boolean | string;
  holdBankReason: string;
  sssNumber: string;
  philHealthNumber: string;
  pagibigNumber: string;
  taxStatus: string;
  dependent: number | string;
  taxId: number | string;
  mobileNumber: number | string;
  telNumber: number | string;
  email: string;
  parentGuardian: string;
  presentAddress: string;
  permanentAddress: string;
}

export class EmployeeProps implements Employee {
  empId: string;
  prevId: string;
  password: string;
  lastName: string;
  firstName: string;
  middleName: string;
  suffixName: string;
  birthday: Date | string;
  gender: string;
  civilStatus: string;
  company: string;
  outlet: number | string;
  area: number | string;
  department: number | string;
  division: number | string;
  position: number | string;
  payPeriod: string;
  billingRate: string;
  workSchedule: string;
  rateTable: string;
  rateMonthly: number | string;
  rateDaily: number | string;
  rateHourly: number | string;
  empStatus: string;
  dateHired: Date | string;
  dateRegularized: Date | string;
  dateResigned: Date | string;
  remarks: string;
  bankAccountNumber: string;
  holdBankProcess: boolean | string;
  holdBankReason: string;
  sssNumber: string;
  philHealthNumber: string;
  pagibigNumber: string;
  taxStatus: string;
  dependent: number | string;
  taxId: number | string;
  mobileNumber: number | string;
  telNumber: number | string;
  email: string;
  parentGuardian: string;
  presentAddress: string;
  permanentAddress: string;

  constructor() {
    this.empId = 'empId';
    this.prevId = 'prevId';
    this.password = 'password';
    this.lastName = 'lastName';
    this.firstName = 'firstName';
    this.middleName = 'middleName';
    this.suffixName = 'suffixName';
    this.birthday = 'birthday';
    this.gender = 'gender';
    this.civilStatus = 'civilStatus';
    this.company = 'company';
    this.outlet = 'outlet';
    this.area = 'area';
    this.department = 'department';
    this.division = 'division';
    this.position = 'position';
    this.payPeriod = 'payPeriod';
    this.billingRate = 'billingRate';
    this.workSchedule = 'workSchedule';
    this.rateTable = 'rateTable';
    this.rateMonthly = 'rateMonthly';
    this.rateDaily = 'rateDaily';
    this.rateHourly = 'rateHourly';
    this.empStatus = 'empStatus';
    this.dateHired = 'dateHired';
    this.dateRegularized = 'dateRegularized';
    this.dateResigned = 'dateResigned';
    this.remarks = 'remarks';
    this.bankAccountNumber = 'bankAccountNumber';
    this.holdBankProcess = 'holdBankProcess';
    this.holdBankReason = 'holdBankReason';
    this.sssNumber = 'sssNumber';
    this.philHealthNumber = 'philHealthNumber';
    this.pagibigNumber = 'pagibigNumber';
    this.taxStatus = 'taxStatus';
    this.dependent = 'dependent';
    this.taxId = 'taxId';
    this.mobileNumber = 'mobileNumber';
    this.telNumber = 'telNumber';
    this.email = 'email';
    this.parentGuardian = 'parentGuardian';
    this.presentAddress = 'presentAddress';
    this.permanentAddress = 'permanentAddress';
  }
}
export class EmployeeViewModel implements ViewModelInterface<Employee> {
  object: Employee = new Employee();
  // foo: Employee[] = [
  //   {
  //     id: '123',
  //     Oldid: '123123',
  //     password: '123123',
  //     LastName: 'asdasd',
  //     FirstName: 'asdasdasd',
  //     MiddleInitial: 'asdasdas',
  //     SuffixName: '123123',
  //     Position: 123,
  //     BankNo: '987654098',
  //     RateDaily: 500
  //   },
  //   {
  //     id: '123',
  //     Oldid: '123123',
  //     password: '123123',
  //     LastName: 'asdasd',
  //     FirstName: 'asdasdasd',
  //     MiddleInitial: 'asdasdas',
  //     SuffixName: '123123',
  //     Position: 123,
  //     BankNo: '987654098',
  //     RateDaily: 500
  //   },
  //   {
  //     id: '123',
  //     Oldid: '123123',
  //     password: '123123',
  //     LastName: 'asdasd',
  //     FirstName: 'asdasdasd',
  //     MiddleInitial: 'asdasdas',
  //     SuffixName: '123123',
  //     Position: 123,
  //     BankNo: '987654098',
  //     RateDaily: 500
  //   }
  // ];

  add(object: Employee) {
    throw new Error('Method not implemented.');
  }
  update(object?: Employee) {
    throw new Error('Method not implemented.');
  }
  delete(id?: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    // return this.foo;
  }
  get(id?: string) {
    throw new Error('Method not implemented.');
  }
}

