﻿import { ViewModelInterface } from '../inteface/model.interface';
export class User {
  id: string;
  fullName: string;
  password: string;
  level: number | string;
  group: string;
  email: string;
  isActive: boolean | string;
}

export class UserProps implements User {
  id: string;
  fullName: string;
  password: string;
  level: string;
  group: string;
  email: string;
  isActive: string;

  constructor() {
    this.id = 'id';
    this.fullName = 'fullName';
    this.password = 'password';
    this.level = 'level';
    this.group = 'group';
    this.email = 'email';
    this.isActive = 'isActive';

  }
}
export class UserViewModel implements ViewModelInterface<User> {

  sampleData: User[] = [
    {
      id: 'Admin',
      password: '1234567',
      fullName: 'Administrator',
      level: 0,
      group: 'System Admin',
      email: 'sample1@mail.com',
      isActive: true
    },
    {
      id: 'Amp',
      password: '1234567',
      fullName: 'Albert M. Pantig',
      level: 1,
      group: 'Credit Management Manager',
      email: 'sample2@mail.com',
      isActive: true
    },
    {
      id: 'Carlo Mark',
      password: '1234567',
      fullName: 'Carlo Mark Ruelan',
      level: 3,
      group: 'Payroll User',
      email: 'sample3@mail.com',
      isActive: false
    }
  ];

  object: User;

  add(object: User) {
    throw new Error('Method not implemented.');
  }
  update(object: User) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    return this.sampleData;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
