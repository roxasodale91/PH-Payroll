﻿import { ViewModelInterface } from '../inteface/model.interface';
export class UserAccessType {
  userID: number | string;
  default: boolean | string;
  typeDescription: string;
  password: string;
  note: string;
}

export class UserAccessTypeProps implements UserAccessType {
  userID: string | number;  default: string | boolean;
  typeDescription: string;
  password: string;
  note: string;

  constructor() {
    this.userID = 'userID';
    this.default = 'default';
    this.typeDescription = 'typeDescription';
    this.password = 'password';
    this.note = 'note';
  }
}
export class UserAccessTypeViewModel implements ViewModelInterface<UserAccessType> {
  object: UserAccessType;

  add(object: UserAccessType) {
    throw new Error('Method not implemented.');
  }
  update(object: UserAccessType) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
