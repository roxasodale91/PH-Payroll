﻿import { ViewModelInterface } from '../inteface/model.interface';
export class Holiday {
  id: string;
  description: string;
  date: Date | string;
  allowedOutlet: string;
  dailyRateId: number | string;
}

export class HolidayProps implements Holiday {
  id: string;  description: string;
  date: string | Date;
  allowedOutlet: string;
  dailyRateId: string | number;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.date = 'date';
    this.allowedOutlet = 'allowedOutlet';
    this.dailyRateId = 'dailyRateId';

  }
}

export class HolidayViewModel implements ViewModelInterface<Holiday> {

  sampleData: Holiday[] = [
    {
      id: '1',
      description: '10TH DAY OF MOHARAM',
      date: '9/30/2017',
      allowedOutlet: '',
      dailyRateId: 1
    },
    {
      id: '2',
      description: '117TH ANNIVERSARY OF MALOLOS CONGRESS',
      date: '9/15/2017',
      allowedOutlet: '258, 312, 373, 393, 165, 179, 233',
      dailyRateId: 1
    }
  ];

  object: Holiday;

  add(object: Holiday) {
    throw new Error('Method not implemented.');
  }
  update(object: Holiday) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
