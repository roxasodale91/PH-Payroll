﻿import { ViewModelInterface } from '../inteface/model.interface';
export class PayrollProcess {
  companyId: string;
  payPeriod: string;
  note: string;
  Locked: boolean | string;
}


export class PayrollProcessProps implements PayrollProcess {
  companyId: string;  payPeriod: string;
  note: string;
  Locked: string | boolean;

  constructor() {
    this.companyId = 'companyId';
    this.payPeriod = 'payPeriod';
    this.note = 'note';
    this.Locked = 'Locked';

  }
}
export class PayrollProcessViewModel implements ViewModelInterface<PayrollProcess> {
  object: PayrollProcess;

  add(object: PayrollProcess) {
    throw new Error('Method not implemented.');
  }
  update(object: PayrollProcess) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
