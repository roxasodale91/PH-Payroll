﻿import { ViewModelInterface } from '../inteface/model.interface';

export class Company {
  id: string;
  companyName: string;
  address: string;
  mobileNo: string;
  telephoneNo: string;
  faxNo: string;
  sss: string;
  philHealth: string;
  tin: string;
  pagIbig: string;
  licenseNo: string;
  email: string;
  contactPerson: string;
  schedTax: number | string;
  schedSSS: number | string;
  schedPhilHealth: number | string;
  schedPagIbig: number | string;
  schedECOLA: number | string;
  refIDTax: number | string;
  refIDSSS: number | string;
  refIDPhilHealth: number | string;
  refIDPagIbig: number | string;
  refIDRegDay: number | string;
  refIDLegHoliday: number | string;
  refIDSpecHoliday: number | string;
  refIDUnWorkHoliday: number | string;
  refIDEcola: number | string;
  isdailyRateFactor: boolean | string;
  dailyRateFactor: number | string;
}

export class CompanyProps implements Company {
  id: string;  companyName: string;
  address: string;
  mobileNo: string;
  telephoneNo: string;
  faxNo: string;
  sss: string;
  philHealth: string;
  tin: string;
  pagIbig: string;
  licenseNo: string;
  email: string;
  contactPerson: string;
  schedTax: string | number;
  schedSSS: string | number;
  schedPhilHealth: string | number;
  schedPagIbig: string | number;
  schedECOLA: string | number;
  refIDTax: string | number;
  refIDSSS: string | number;
  refIDPhilHealth: string | number;
  refIDPagIbig: string | number;
  refIDRegDay: string | number;
  refIDLegHoliday: string | number;
  refIDSpecHoliday: string | number;
  refIDUnWorkHoliday: string | number;
  refIDEcola: string | number;
  isdailyRateFactor: string | boolean;
  dailyRateFactor: string | number;

  constructor() {
    this.id = 'id';
    this.companyName = 'companyName';
    this.address = 'address';
    this.mobileNo = 'mobileNo';
    this.telephoneNo = 'telephoneNo';
    this.faxNo = 'faxNo';
    this.sss = 'sss';
    this.philHealth = 'philHealth';
    this.tin = 'tin';
    this.pagIbig = 'pagIbig';
    this.licenseNo = 'licenseNo';
    this.email = 'email';
    this.contactPerson = 'contactPerson';
    this.schedTax = 'schedTax';
    this.schedSSS = 'schedSSS';
    this.schedPhilHealth = 'schedPhilHealth';
    this.schedPagIbig = 'schedPagIbig';
    this.schedECOLA = 'schedECOLA';
    this.refIDTax = 'refIDTax';
    this.refIDSSS = 'refIDSSS';
    this.refIDPhilHealth = 'refIDPhilHealth';
    this.refIDPagIbig = 'refIDPagIbig';
    this.refIDRegDay = 'refIDRegDay';
    this.refIDLegHoliday = 'refIDLegHoliday';
    this.refIDSpecHoliday = 'refIDSpecHoliday';
    this.refIDUnWorkHoliday = 'refIDUnWorkHoliday';
    this.refIDEcola = 'refIDEcola';
    this.isdailyRateFactor = 'isdailyRateFactor';
    this.dailyRateFactor = 'dailyRateFactor';

  }
}

export class CompanyViewModel implements ViewModelInterface<Company> {
  object: Company;  add(object: Company) {
    throw new Error('Method not implemented.');
  }
  update(object: Company) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
