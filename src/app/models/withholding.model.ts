import { ViewModelInterface } from '../inteface/model.interface';

export class WithHolding {
  taxType: string;
  exemptStatus: string;
  exemptAmount: string | number;
  amountOne: string | number;
  overOne: string | number;
  percentOne: string | number;
  amountTwo: string | number;
  overTwo: string | number;
  percentTwo: string | number;
}

export class WithHoldingProps implements WithHolding {
  taxType: string;
  exemptStatus: string;
  exemptAmount: string | number;
  amountOne: string | number;
  overOne: string | number;
  percentOne: string | number;
  amountTwo: string | number;
  overTwo: string | number;
  percentTwo: string | number;

  constructor() {
    this.taxType = 'taxType';
    this. exemptStatus = 'exemptStatus';
    this.exemptAmount = 'exemptAmount';
    this.amountOne = 'amount1';
    this.overOne = 'over1';
    this.percentOne = 'percent1';
    this.amountTwo = 'amount2';
    this.overTwo = 'over2';
    this.percentTwo = 'percent2';
  }
}

export class WithHoldingViewModel implements ViewModelInterface<WithHolding> {
  object: WithHolding;
  add(object: WithHolding) {
    throw new Error('Method not implemented.');
  }
  update(object: WithHolding) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}

