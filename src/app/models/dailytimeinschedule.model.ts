﻿import { ViewModelInterface } from '../inteface/model.interface';
export class DailyTimeInSchedule {
  id: string;
  scheduleDescription: string;
  allowedGracePeriod: number | string;
  mondayEnabled: boolean | string;
  mondayStart: Date | string;
  mondayEnd: Date | string;
  mondayStartBreak: Date | string;
  mondayEndBreak: Date | string;
  tuesdayEnabled: boolean | string;
  tuesdayStart: Date | string;
  tuesdayEnd: Date | string;
  tuesdayStartBreak: Date | string;
  tuesdayEndBreak: Date | string;
  wednesdayEnabled: boolean | string;
  wednesdayStart: Date | string;
  wednesdayEnd: Date | string;
  wednesdayStartBreak: Date | string;
  wednesdayEndBreak: Date | string;
  thursdayEnabled: boolean | string;
  thursdayStart: Date | string;
  thursdayEnd: Date | string;
  thursdayStartBreak: Date | string;
  thursdayEndBreak: Date | string;
  fridayEnabled: boolean | string;
  fridayStart: Date | string;
  fridayEnd: Date | string;
  fridayStartBreak: Date | string;
  fridayEndBreak: Date | string;
  saturdayEnabled: boolean | string;
  saturdayStart: Date | string;
  saturdayEnd: Date | string;
  saturdayStartBreak: Date | string;
  saturdayEndBreak: Date | string;
  sundayEnabled: boolean | string;
  sundayStart: Date | string;
  sundayEnd: Date | string;
  sundayStartBreak: Date | string;
  sundayEndBreak: Date | string;
}

export class DailyTimeInScheduleProps implements DailyTimeInSchedule {
  id: string;  scheduleDescription: string;
  allowedGracePeriod: string | number;
  mondayEnabled: boolean | string;
  mondayStart: string | Date;
  mondayEnd: string | Date;
  mondayStartBreak: string | Date;
  mondayEndBreak: string | Date;
  tuesdayEnabled: boolean | string;
  tuesdayStart: string | Date;
  tuesdayEnd: string | Date;
  tuesdayStartBreak: string | Date;
  tuesdayEndBreak: string | Date;
  wednesdayEnabled: boolean | string;
  wednesdayStart: string | Date;
  wednesdayEnd: string | Date;
  wednesdayStartBreak: string | Date;
  wednesdayEndBreak: string | Date;
  thursdayEnabled: boolean | string;
  thursdayStart: string | Date;
  thursdayEnd: string | Date;
  thursdayStartBreak: string | Date;
  thursdayEndBreak: string | Date;
  fridayEnabled: boolean | string;
  fridayStart: string | Date;
  fridayEnd: string | Date;
  fridayStartBreak: string | Date;
  fridayEndBreak: string | Date;
  saturdayEnabled: boolean | string;
  saturdayStart: string | Date;
  saturdayEnd: string | Date;
  saturdayStartBreak: string | Date;
  saturdayEndBreak: string | Date;
  sundayEnabled: boolean | string;
  sundayStart: string | Date;
  sundayEnd: string | Date;
  sundayStartBreak: string | Date;
  sundayEndBreak: string | Date;

  constructor() {
    this.id = 'id';
    this.scheduleDescription = 'scheduleDescription';
    this.allowedGracePeriod = 'allowedGracePeriod';
    this.mondayEnabled = 'mondayEnabled';
    this.mondayStart = 'mondayStart';
    this.mondayEnd = 'mondayEnd';
    this.mondayStartBreak = 'mondayStartBreak';
    this.mondayEndBreak = 'mondayEndBreak';
    this.tuesdayEnabled = 'tuesdayEnabled';
    this.tuesdayStart = 'tuesdayStart';
    this.tuesdayEnd = 'tuesdayEnd';
    this.tuesdayStartBreak = 'tuesdayStartBreak';
    this.tuesdayEndBreak = 'tuesdayEndBreak';
    this.wednesdayEnabled = 'wednesdayEnabled';
    this.wednesdayStart = 'wednesdayStart';
    this.wednesdayEnd = 'wednesdayEnd';
    this.wednesdayStartBreak = 'wednesdayStartBreak';
    this.wednesdayEndBreak = 'wednesdayEndBreak';
    this.thursdayEnabled = 'thursdayEnabled';
    this.thursdayStart = 'thursdayStart';
    this.thursdayEnd = 'thursdayEnd';
    this.thursdayStartBreak = 'thursdayStartBreak';
    this.thursdayEndBreak = 'thursdayEndBreak';
    this.fridayEnabled = 'fridayEnabled';
    this.fridayStart = 'fridayStart';
    this.fridayEnd = 'fridayEnd';
    this.fridayStartBreak = 'fridayStartBreak';
    this.fridayEndBreak = 'fridayEndBreak';
    this.saturdayEnabled = 'saturdayEnabled';
    this.saturdayStart = 'saturdayStart';
    this.saturdayEnd = 'saturdayEnd';
    this.saturdayStartBreak = 'saturdayStartBreak';
    this.saturdayEndBreak = 'saturdayEndBreak';
    this.sundayEnabled = 'sundayEnabled';
    this.sundayStart = 'sundayStart';
    this.sundayEnd = 'sundayEnd';
    this.sundayStartBreak = 'sundayStartBreak';
    this.sundayEndBreak = 'sundayEndBreak';
  }

}

export class DailyTimeInScheduleViewModel implements ViewModelInterface<DailyTimeInSchedule> {
  object: DailyTimeInSchedule;

  add(object: DailyTimeInSchedule) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyTimeInSchedule) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
