﻿import { ViewModelInterface } from '../inteface/model.interface';

export class UserRolePOS {
  roleId: number | string;
  securityLevel: number | string;
  roleDescription: string;
  newOrder: boolean | string;
  voidOrder: boolean | string;
  reOrder: boolean | string;
  refundOrder: boolean | string;
  cancelOrder: boolean | string;
  discountOrder: boolean | string;
  changeSegment: boolean | string;
  certificatePayment: boolean | string;
  chargePayment: boolean | string;
  creditPayment: boolean | string;
  debitPayment: boolean | string;
  openMemo: boolean | string;
  closeTransaction: boolean | string;
  openCashDrawer: boolean | string;
  foodServerTransfer: boolean | string;
  transferTable: boolean | string;
  mergeTable: boolean | string;
  splitTable: boolean | string;
  splitCheck: boolean | string;
  mergeCheck: boolean | string;
  headCount: boolean | string;
  guestInfo: boolean | string;
  printBill: boolean | string;
  reprintInvoice: boolean | string;
  viewReport: boolean | string;
  timeKeeping: boolean | string;
  closeTerminal: boolean | string;
  startOfDay: boolean | string;
  endOfDay: boolean | string;
  reprintOrderSlip: boolean | string;
  tableLayout: boolean | string;
}

export class UserRolePOSProps implements UserRolePOS {
  roleId: string | number;  securityLevel: string | number;
  roleDescription: string;
  newOrder: string | boolean;
  voidOrder: string | boolean;
  reOrder: string | boolean;
  refundOrder: string | boolean;
  cancelOrder: string | boolean;
  discountOrder: string | boolean;
  changeSegment: string | boolean;
  certificatePayment: string | boolean;
  chargePayment: string | boolean;
  creditPayment: string | boolean;
  debitPayment: string | boolean;
  openMemo: string | boolean;
  closeTransaction: string | boolean;
  openCashDrawer: string | boolean;
  foodServerTransfer: string | boolean;
  transferTable: string | boolean;
  mergeTable: string | boolean;
  splitTable: string | boolean;
  splitCheck: string | boolean;
  mergeCheck: string | boolean;
  headCount: string | boolean;
  guestInfo: string | boolean;
  printBill: string | boolean;
  reprintInvoice: string | boolean;
  viewReport: string | boolean;
  timeKeeping: string | boolean;
  closeTerminal: string | boolean;
  startOfDay: string | boolean;
  endOfDay: string | boolean;
  reprintOrderSlip: string | boolean;
  tableLayout: string | boolean;

  constructor() {
    this.roleId = 'roleId';
    this.securityLevel = 'securityLevel';
    this.roleDescription = 'roleDescription';
    this.newOrder = 'newOrder';
    this.voidOrder = 'voidOrder';
    this.reOrder = 'reOrder';
    this.refundOrder = 'refundOrder';
    this.cancelOrder = 'cancelOrder';
    this.discountOrder = 'discountOrder';
    this.changeSegment = 'changeSegment';
    this.certificatePayment = 'certificatePayment';
    this.chargePayment = 'chargePayment';
    this.creditPayment = 'creditPayment';
    this.debitPayment = 'debitPayment';
    this.openMemo = 'openMemo';
    this.closeTransaction = 'closeTransaction';
    this.openCashDrawer = 'openCashDrawer';
    this.foodServerTransfer = 'foodServerTransfer';
    this.transferTable = 'transferTable';
    this.mergeTable = 'mergeTable';
    this.splitTable = 'splitTable';
    this.splitCheck = 'splitCheck';
    this.mergeCheck = 'mergeCheck';
    this.headCount = 'headCount';
    this.guestInfo = 'guestInfo';
    this.printBill = 'printBill';
    this.reprintInvoice = 'reprintInvoice';
    this.viewReport = 'viewReport';
    this.timeKeeping = 'timeKeeping';
    this.closeTerminal = 'closeTerminal';
    this.startOfDay = 'startOfDay';
    this.endOfDay = 'endOfDay';
    this.reprintOrderSlip = 'reprintOrderSlip';
    this.tableLayout = 'tableLayout';

  }
}

export class UserRolePOSViewModel implements ViewModelInterface<UserRolePOS> {
  object: UserRolePOS;

  add(object: UserRolePOS) {
    throw new Error('Method not implemented.');
  }
  update(object: UserRolePOS) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
