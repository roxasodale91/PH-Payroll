﻿import { ViewModelInterface } from '../inteface/model.interface';

export class DailyTimeIn {
  empId: string;
  companyId: string;
  outletId: number | string;
  payPeriod: string;
  TxDate: Date | string;
  DailyRateid: number | string;
  Reg_Hour: number | string;
  OT_Hour: number | string;
  OTND_Hour: number | string;
  NightDiff_Hour: number | string;
  Absent_Hour: number | string;
  updateBy: string;
  updateDate: Date | string;
}

export class DailyTimeinProps implements DailyTimeIn {
  empId: string;  companyId: string;
  outletId: string | number;
  payPeriod: string;
  TxDate: string | Date;
  DailyRateid: string | number;
  Reg_Hour: string | number;
  OT_Hour: string | number;
  OTND_Hour: string | number;
  NightDiff_Hour: string | number;
  Absent_Hour: string | number;
  updateBy: string;
  updateDate: string | Date;

  constructor() {
    this.empId = 'empId';
    this.companyId = 'companyId';
    this.outletId = 'outletId';
    this.payPeriod = 'payPeriod';
    this.TxDate = 'TxDate';
    this.DailyRateid = 'DailyRateid';
    this.Reg_Hour = 'Reg_Hour';
    this.OT_Hour = 'OT_Hour';
    this.OTND_Hour = 'OTND_Hour';
    this.NightDiff_Hour = 'NightDiff_Hour';
    this.Absent_Hour = 'Absent_Hour';
    this.updateBy = 'updateBy';
    this.updateDate = 'updateDate';

  }

}
export class DailyTimeInViewMOdel implements ViewModelInterface<DailyTimeIn> {
  object: DailyTimeIn;

  add(object: DailyTimeIn) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyTimeIn) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
