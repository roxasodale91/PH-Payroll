﻿import { ViewModelInterface } from '../inteface/model.interface';
export class PagIbig {
  id: string;
  description: string;
  rangeStart: number | string;
  rangeEnd: number | string;
  ER: number | string;
  EE: number | string;
  total: number | string;
}


export class PagIbigProps implements PagIbig {
  id: string;  description: string;
  rangeStart: string | number;
  rangeEnd: string | number;
  ER: string | number;
  EE: string | number;
  total: string | number;

    constructor() {
      this.id = 'id';
      this.description = 'description';
      this.rangeStart = 'Range_Start';
      this.rangeEnd = 'Range_End';
      this.ER = 'ER';
      this.EE = 'EE';
      this.total = 'Total';
    }

}
export class PagIbigViewModel implements ViewModelInterface<PagIbig> {

  sampleData: PagIbig[] = [
    {
      id: '1',
      description: '0.00 - 1499.99',
      rangeStart: 0.00,
      rangeEnd: 1499.99,
      ER: 2.00,
      EE: 1.00,
      total: 3.00
    },
    {
      id: '2',
      description: '1500 - 9999999.99',
      rangeStart: 1500.00,
      rangeEnd: 9999999.99,
      ER: 2.00,
      EE: 2.00,
      total: 4.00
    }
  ];

  object: PagIbig;

  add(object: PagIbig) {
    throw new Error('Method not implemented.');
  }
  update(object: PagIbig) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    return this.sampleData;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
