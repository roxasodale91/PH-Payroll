﻿import { ViewModelInterface } from '../inteface/model.interface';
export class SSS {
  id: string;
  description: string;
  rocStart: number | string;
  rocEnd: number | string;
  MSC: number | string;
  SSER: number | string;
  SSEE: number | string;
  ECER: number | string;
  SsTotal: number | string;
}

export class SSSProps implements SSS {
  id: string;  description: string;
  rocStart: string | number;
  rocEnd: string | number;
  MSC: string | number;
  SSER: string | number;
  SSEE: string | number;
  ECER: number | string;
  SsTotal: string | number;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.rocStart = 'ROC_Start';
    this.rocEnd = 'ROC_End';
    this.MSC = 'MSC';
    this.SSER = 'SSER';
    this.SSEE = 'SSEE';
    this.ECER = 'ECER';
    this.SsTotal = 'SSTotal';
  }
}
export class SSSViewModel implements ViewModelInterface<SSS> {
  object: SSS;

  add(object: SSS) {
    throw new Error('Method not implemented.');
  }
  update(object: SSS) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    // return this.sampleData;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
