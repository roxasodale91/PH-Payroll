import { OnInit } from '@angular/core';
import { Deserializable } from './deserializable.model';

export class Sample {

     name: string;
     address: string;
     zip?: number | string;
     cityname?: string;
     street?: string;
}

export class SamplePropertyMeta implements Sample {
    name: string;    address: string;
    zip?: string | number;
    cityname?: string;
    street?: string;

    constructor() {
        this.name = 'name';
        this.address = 'address';
        this.cityname = 'cityname';
        this.zip = 'zip';
        this.street = 'street';
    }

}

export class SampleMethods {

    sample: Sample[] = [
        {name: 'hh', address: 'aaa'},
        {name: 'hha', address: 'aaas'}
    ];

    res = '';

    propName(prop, value) {
      for (const i in prop) {
          if (typeof prop[i] === 'object') {
              if (this.propName(prop[i], value)) {
                  return this.res;
              }
          } else {
              if (prop[i] === value) {
                this.res = i;
                  return this.res;
              }
          }
      }
      return undefined;
    }

}

