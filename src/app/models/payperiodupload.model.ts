﻿import { ViewModelInterface } from '../inteface/model.interface';
export class PayPeriodUpload {
  recId: string;
  companyId: string;
  payPeriod: string;
  id: number | string;
  empId: number | string;
  fullName: string;
  outlet: number | string;
  txTypeid: number | string;
  txTypeDesc: string;
  txId: number | string;
  txDesc: string;
  amount: number | string;
  note: string;
}

export class PayPeriodUploadProps implements PayPeriodUpload {
  recId: string;  companyId: string;
  payPeriod: string;
  id: string | number;
  empId: string | number;
  fullName: string;
  outlet: string | number;
  txTypeid: string | number;
  txTypeDesc: string;
  txId: string | number;
  txDesc: string;
  amount: string | number;
  note: string;

  constructor() {
    this.recId = 'recId';
    this.companyId = 'companyId';
    this.payPeriod = 'payPeriod';
    this.id = 'id';
    this.empId = 'empId';
    this.fullName = 'fullName';
    this.outlet = 'outlet';
    this.txTypeid = 'txTypeid';
    this.txTypeDesc = 'txTypeDesc';
    this.txId = 'txId';
    this.txDesc = 'txDesc';
    this.amount = 'amount';
    this.note = 'note';

  }
}
export class PayPeriodUploadViewModel implements ViewModelInterface <PayPeriodUpload> {
  object: PayPeriodUpload;

  add(object: PayPeriodUpload) {
    throw new Error('Method not implemented.');
  }
  update(object: PayPeriodUpload) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
