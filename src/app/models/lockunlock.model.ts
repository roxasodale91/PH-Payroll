import { ViewModelInterface } from '../inteface/model.interface';

export class LockUnlock {
  payPeriod: string;
  note: string;
  isLocked: boolean | string;
}

export class LockUnlockProps implements LockUnlock {
  payPeriod: string;  note: string;
  isLocked: string | boolean;

  constructor() {
    this.payPeriod = 'payPeriod';
    this.note = 'note';
    this.isLocked = 'isLocked';
  }
}

export class LockUnlockViewmodel implements ViewModelInterface<LockUnlock> {
  object: LockUnlock;
  add(object: LockUnlock) {
    throw new Error('Method not implemented.');
  }
  update(object: LockUnlock) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
