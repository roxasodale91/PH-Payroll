﻿import { ViewModelInterface } from '../inteface/model.interface';
export class Deduction {
  recId: string;
  empId: string;
  fullName: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  txDate: Date | string;
  loanDate: Date | string;
  amount: number | string;
  deductionId: number | string;
  deductionType: number | string;
  scheduleMode: string;
  stopReason: string;
  terms: number | string;
  note: string;
  updateBy: string;
  updateDate: Date | string;
  locked: boolean | string;
}

export class DeductionProps implements Deduction {
  recId: string;
  empId: string;
  fullName: string;
  companyId: string;
  outletId: number | string;
  outletName: string;
  payPeriod: string;
  txDate: Date | string;
  loanDate: Date | string;
  amount: number | string;
  deductionId: number | string;
  deductionType: number | string;
  scheduleMode: string;
  stopReason: string;
  terms: number | string;
  note: string;
  updateBy: string;
  updateDate: Date | string;
  locked: boolean | string;

  constructor() {
  this.recId = 'recId';
  this.empId = 'empId';
  this.fullName = 'fullName';
  this.companyId = 'companyId';
  this.outletId = 'outletId';
  this.outletName = 'outletName';
  this.payPeriod = 'payPeriod';
  this.txDate = 'txDate';
  this.loanDate = 'loanDate';
  this.amount = 'amount';
  this.deductionId = 'deductionId';
  this.deductionType = 'deductionType';
  this.scheduleMode = 'scheduleMode';
  this.stopReason = 'stopReason';
  this.terms = 'terms';
  this.note = 'note';
  this.updateBy = 'updateBy';
  this.updateDate = 'updateDate';
  this.locked = 'locked';

  }
}
export class DeductionViewModel implements ViewModelInterface<Deduction> {
  object: Deduction;

  add(object: Deduction) {
    throw new Error('Method not implemented.');
  }
  update(object: Deduction) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }

}
