﻿import { ViewModelInterface } from '../inteface/model.interface';
export class CompanyPosition {
  id: number | string;
  description: string;
  role: string;
}

export class CompanyPositionProps implements CompanyPosition {
  id: string | number;
  description: string;
  role: string;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.role = 'role';

  }
}

export class CompanyPositionViewModel implements ViewModelInterface<CompanyPosition> {
  object: CompanyPosition;

  add(object: CompanyPosition) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanyPosition) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
