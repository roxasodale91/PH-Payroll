﻿import { ViewModelInterface } from '../inteface/model.interface';

export class CompanyArea {
  id: number | string;
  description: string;
  location: string;
}

export class CompanyAreaProps implements CompanyArea {
  id: string | number;
  description: string;
  location: string;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.location = 'location';
  }
}

export class CompanyAreaViewModel implements ViewModelInterface<CompanyArea> {

  foo: CompanyArea[] = [
    {id: 123, description: 'aaa', location: 'aaa'},
    {id: 456, description: 'aaa', location: 'aaa'},
    {id: 789, description: 'aaa', location: 'aaa'},
    {id: 123, description: 'aaa', location: 'aaa'}
  ];

  object: CompanyArea = new CompanyArea();

  add(object: CompanyArea) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanyArea) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    return this.foo;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }

}
