﻿import { ViewModelInterface } from '../inteface/model.interface';

export class UserAccess {
  group: string;
  level: number | string;
  sysUser: boolean | string;
  sysGroup: boolean | string;
  modARE: boolean | string;
  modHRIS: boolean | string;
  company: boolean | string;
  cmpEmployee: boolean | string;
  cmpArea: boolean | string;
  cmpOutlet: boolean | string;
  cmpDepartment: boolean | string;
  cmpDivision: boolean | string;
  cmpPosition: boolean | string;
  cmpSalaryTable: boolean | string;
  mntTax: boolean | string;
  mntPhilHealth: boolean | string;
  mntSSS: boolean | string;
  mntPagIbig: boolean | string;
  mntDailyRate: boolean | string;
  mntDeductions: boolean | string;
  mntEarnings: boolean | string;
  mntBillingRate: boolean | string;
  mntLeave: boolean | string;
  mntHoliday: boolean | string;
  mntDailySched: boolean | string;
  trsDeductions: boolean | string;
  trsEarnings: boolean | string;
  trsLeave: boolean | string;
  trsTimekeeping: boolean | string;
  trsProcessPayroll: boolean | string;
  trspayPeriod: boolean | string;
  rptReportCenter: boolean | string;
  rptBankFileBPI: boolean | string;
  tlsTimekeeping: boolean | string;
  tlsTransaction: boolean | string;
  tlsLeave: boolean | string;
  sysUpdateInfo: boolean | string;
}


export class UserAccessProps implements UserAccess {
  group: string;  level: string | number;
  sysUser: string | boolean;
  sysGroup: string | boolean;
  modARE: string | boolean;
  modHRIS: string | boolean;
  company: string | boolean;
  cmpEmployee: string | boolean;
  cmpArea: string | boolean;
  cmpOutlet: string | boolean;
  cmpDepartment: string | boolean;
  cmpDivision: string | boolean;
  cmpPosition: string | boolean;
  cmpSalaryTable: string | boolean;
  mntTax: string | boolean;
  mntPhilHealth: string | boolean;
  mntSSS: string | boolean;
  mntPagIbig: string | boolean;
  mntDailyRate: string | boolean;
  mntDeductions: string | boolean;
  mntEarnings: string | boolean;
  mntBillingRate: string | boolean;
  mntLeave: string | boolean;
  mntHoliday: string | boolean;
  mntDailySched: string | boolean;
  trsDeductions: string | boolean;
  trsEarnings: string | boolean;
  trsLeave: string | boolean;
  trsTimekeeping: string | boolean;
  trsProcessPayroll: string | boolean;
  trspayPeriod: string | boolean;
  rptReportCenter: string | boolean;
  rptBankFileBPI: string | boolean;
  tlsTimekeeping: string | boolean;
  tlsTransaction: string | boolean;
  tlsLeave: string | boolean;
  sysUpdateInfo: string | boolean;

  constructor() {
    this.group = 'group';
    this.level = 'level';
    this.sysUser = 'sysUser';
    this.sysGroup = 'sysGroup';
    this.modARE = 'modARE';
    this.modHRIS = 'modHRIS';
    this.company = 'company';
    this.cmpEmployee = 'cmpEmployee';
    this.cmpArea = 'cmpArea';
    this.cmpOutlet = 'cmpOutlet';
    this.cmpDepartment = 'cmpDepartment';
    this.cmpDivision = 'cmpDivision';
    this.cmpPosition = 'cmpPosition';
    this.cmpSalaryTable = 'cmpSalaryTable';
    this.mntTax = 'mntTax';
    this.mntPhilHealth = 'mntPhilHealth';
    this.mntSSS = 'mntSSS';
    this.mntPagIbig = 'mntPagIbig';
    this.mntDailyRate = 'mntDailyRate';
    this.mntDeductions = 'mntDeductions';
    this.mntEarnings = 'mntEarnings';
    this.mntBillingRate = 'mntBillingRate';
    this.mntLeave = 'mntLeave';
    this.mntHoliday = 'mntHoliday';
    this.mntDailySched = 'mntDailySched';
    this.trsDeductions = 'trsDeductions';
    this.trsEarnings = 'trsEarnings';
    this.trsLeave = 'trsLeave';
    this.trsTimekeeping = 'trsTimekeeping';
    this.trsProcessPayroll = 'trsProcessPayroll';
    this.trspayPeriod = 'trspayPeriod';
    this.rptReportCenter = 'rptReportCenter';
    this.rptBankFileBPI = 'rptBankFileBPI';
    this.tlsTimekeeping = 'tlsTimekeeping';
    this.tlsTransaction = 'tlsTransaction';
    this.tlsLeave = 'tlsLeave';
    this.sysUpdateInfo = 'sysUpdateInfo';

  }
}
export class UserAccessViewModel implements ViewModelInterface<UserAccess> {
  object: UserAccess;

  add(object: UserAccess) {
    throw new Error('Method not implemented.');
  }
  update(object: UserAccess) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }

}
