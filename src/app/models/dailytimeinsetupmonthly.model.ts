﻿import { ViewModelInterface } from '../inteface/model.interface';
export class DailyTimeInSetupMonthly {
  payPeriod: string;
  companyId: string;
  updateBy: string;
  updateDate: Date | string;
}

export class DailyTimeInSetupMonthlyProps implements DailyTimeInSetupMonthly {
  payPeriod: string;  companyId: string;
  updateBy: string;
  updateDate: string | Date;

  constructor() {
    this.payPeriod = 'payPeriod';
    this.companyId = 'companyId';
    this.updateBy = 'updateBy';
    this.updateDate = 'updateDate';
  }
}
export class DailyTimeInSetupMonthlyViewModel implements ViewModelInterface<DailyTimeInSetupMonthly> {
  object: DailyTimeInSetupMonthly;

  add(object: DailyTimeInSetupMonthly) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyTimeInSetupMonthly) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
