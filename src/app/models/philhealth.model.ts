﻿import { ViewModelInterface } from '../inteface/model.interface';
export class PhilHealth {
  id: string;
  description: string;
  rangeEnd: number | string;
  ER: number | string;
  EE: number | string;
  total: number | string;
  percent: number | string;
}

export class PhilHealthProps implements PhilHealth {
  id: string;  description: string;
  rangeEnd: string | number;
  ER: string | number;
  EE: string | number;
  total: number | string;
  percent: string | number;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.rangeEnd = 'Range_End';
    this.ER = 'ER';
    this.EE = 'EE';
    this.total = 'Total';
    this.percent = 'Percent';

  }
}

export class PhilHealthViewModel implements ViewModelInterface<PhilHealth> {

  sampleData: PhilHealth[] = [
    {
      id: '1',
      description: '10,000 and below',
      rangeEnd: 0.00,
      ER: 137.50,
      EE: 137.50,
      total: 137.50 + 137.50,
      percent: 0.00
    },
    {
      id: '2',
      description: '10,000.01 - 39,999.99',
      rangeEnd: 10000.01,
      ER: 137.51,
      EE: 137.51,
      total: 137.51 + 137.51,
      percent: 2.75
    },
    {
      id: '28',
      description: '40,000 and up',
      rangeEnd: 40000.00,
      ER: 550.00,
      EE: 550.00,
      total: 550 + 550,
      percent: 0.00
    }
  ];

  object: PhilHealth = new PhilHealth();

  add(object: PhilHealth) {
    throw new Error('Method not implemented.');
  }
  update(object: PhilHealth) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    return this.sampleData;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
