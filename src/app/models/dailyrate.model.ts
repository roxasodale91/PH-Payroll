﻿import { ViewModelInterface } from '../inteface/model.interface';
export class DailyRate {
  id: string;
  description: string;
  BASC: number | string;
  BSND: number | string;
  BSOT: number | string;
  OTND: number | string;
  FHRS: number | string;
  FPLS: number | string;
  SPLS: number | string;
}

export class DailyRateProps implements DailyRate {
  id: string;  description: string;
  BASC: string | number;
  BSND: string | number;
  BSOT: string | number;
  OTND: string | number;
  FHRS: string | number;
  FPLS: string | number;
  SPLS: string | number;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.BASC = 'BASC';
    this.BSND = 'BSND';
    this.BSOT = 'BSOT';
    this.OTND = 'OTND';
    this.FHRS = 'FHRS';
    this.FPLS = 'FPLS';
    this.SPLS = 'SPLS';
  }

}
export class DailyRateViewmodel implements ViewModelInterface<DailyRate> {

  sampleData: DailyRate[] = [
    {
      id: '1',
      description: 'UNWORKED HOLIDAY PAY',
      BASC: 1,
      BSND: 0,
      BSOT: 0,
      OTND: 0,
      FHRS: 0,
      FPLS: 0.0000,
      SPLS: 0.0000
    },
    {
      id: '2',
      description: 'LEGAL HOLIDAY',
      BASC: 2,
      BSND: 0.2,
      BSOT: 2.6,
      OTND: 0.26,
      FHRS: 0,
      FPLS: 0.0000,
      SPLS: 0.0000
    }
  ];

  object: DailyRate;  add(object: DailyRate) {
    throw new Error('Method not implemented.');
  }
  update(object: DailyRate) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    return this.sampleData;
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
