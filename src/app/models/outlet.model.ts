﻿import { ViewModelInterface } from '../inteface/model.interface';
export class CompanyOutlet {
  id: number | string;
  description: string;
  areaId: number | string;
  corporateNameId: number | string;
  eclLowLimit: number | string;
  eclUpperLimit: number | string;
  eclDayAmount: number | string;
  eclMinWage: number | string;
  storeCode: string;
}

export class CompanyOutletProps implements CompanyOutlet {
  id: number | string;
  description: string;
  areaId: number | string;
  corporateNameId: number | string;
  eclLowLimit: number | string;
  eclUpperLimit: number | string;
  eclDayAmount: number | string;
  eclMinWage: number | string;
  storeCode: string;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.areaId = 'areaId';
    this.corporateNameId = 'corporateNameId';
    this.eclLowLimit = 'eclLowLimit';
    this.eclUpperLimit = 'eclUpperLimit';
    this.eclDayAmount = 'eclDayAmount';
    this.eclMinWage = 'eclMinWage';
    this.storeCode = 'storeCode';

  }
}

export class CompanyOutletViewModel implements ViewModelInterface<CompanyOutlet> {
  object: CompanyOutlet;

  add(object: CompanyOutlet) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanyOutlet) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }
}
