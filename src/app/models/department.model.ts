﻿import { ViewModelInterface } from '../inteface/model.interface';


export class CompanyDepartment {
  id: number | string;
  description: string;
  note: string;
  outletId: number | string;
}

export class CompanyDepartmentProps implements CompanyDepartment {
  id: string | number;
  description: string;
  note: string;
  outletId: string | number;

  constructor() {
    this.id = 'id';
    this.description = 'description';
    this.note = 'note';
    this.outletId = 'outletId';
  }
}
export class CompanyDepartmentViewModel implements ViewModelInterface<CompanyDepartment> {
  object: CompanyDepartment;

  add(object: CompanyDepartment) {
    throw new Error('Method not implemented.');
  }
  update(object: CompanyDepartment) {
    throw new Error('Method not implemented.');
  }
  delete(id: string) {
    throw new Error('Method not implemented.');
  }
  getAll() {
    throw new Error('Method not implemented.');
  }
  get(id: string) {
    throw new Error('Method not implemented.');
  }


}
