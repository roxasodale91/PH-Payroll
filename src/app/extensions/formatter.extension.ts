import { Observable, BehaviorSubject } from 'rxjs';
import { CompanyArea } from '../models/area.model';

export class ExtensionService {


  returnFiltered: any[];
  private returnSubject: BehaviorSubject<any[]>;


  constructor() {
    this.returnFiltered = [];
    this.returnSubject = new BehaviorSubject(this.returnFiltered);
    // this.returnSubject.next(Object.assign({}, this.returnFiltered));
  }

  format(entity: Observable<any[]>) {
    entity.subscribe((data: any[]) => {
      for (let i = 0; data.length > i; i++) {
        this.returnFiltered.push({
          value: data[i].id.toString(),
          label: data[i].description.toString()
        });
      }
      this.returnSubject.next(this.returnFiltered);
    });
  }
  get formatted() {
    return this.returnSubject.asObservable();
  }
}
